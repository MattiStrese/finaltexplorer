function [] = FinalTexplorer_MAIN(doRecording,plotIt,ttitle, varargin)

clc
close all
if( nargin < 3), help FinalTexplorer_MAIN;
    return;
end

% Check, if FinalTexplorer_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    FinalTexplorer_Constants
end


if doRecording == 1
    [materialRecording]=FinalTexplorer_DoSingleMeasurement( 2,10,0,1, 'Exploration', 1 );
end

[ db ] = FinalTexplorer_LoadDatabase( );

for k=1:1
    [tactileFeatures] = FinalTexplorer_ProcessQuery(db{k}.data,db{k}.name,plotIt);
    db{k}.featureVector = tactileFeatures;
end
syntouchStandardPlot(db{1}.featureVector ,db{1}.name);
 
 
 
 
 
 
 
 

end

