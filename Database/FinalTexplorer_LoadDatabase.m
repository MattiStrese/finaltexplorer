function [ db ] = FinalTexplorer_LoadDatabase( )

% Ensure correct number of inputs
if( nargin~=  0), help FinalTexplorer_LoadDatabase;
    return;
end;

% Check, if CBTR_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    FinalTexplorer_Constants
end


%%
%  output
db = {};


materialRecordingFiles              = dir(strcat(materialMatPath,'*.mat'));


perc = 0;
for i = 1:numel(materialRecordingFiles)
    %clc
    disp(strcat(num2str(perc), ' % material recordings loaded'));
    materialRecordingFiles(i).name
    tmp = load(strcat(materialMatPath,materialRecordingFiles(i).name));
    material = tmp.materialRecording;
    
    %strTrain = strcat(soundPath,'Training\\',soundFiles(i).name);
    %strTest = strcat(soundPath,'Testing\\',soundFiles(i).name);
    
    db{i}.data = material;
    db{i}.name = y{i};
    
    
    perc = i*100/numel(materialRecordingFiles);
    
end








end

































% accelPath               = strcat(dbPath,'AccelScansComponents\\Movement\\');
% accelTapPath            = strcat(dbPath,'AccelScansComponents\\Tapping\\');
% accelTapPathSingleTap   = strcat(dbPath,'AccelScansComponents\\SingleTappingImpulses\\');
%
% soundPath               = strcat(dbPath,'SoundScans\\Movement\\');
% soundTapPath            = strcat(dbPath,'SoundScans\\Tapping\\');
% soundTapPathSingleTap   = strcat(dbPath,'SoundScans\\SingleTappingImpulses\\');
%
% %imagePath               = strcat(pwd,'\\Database\\ImageMagnified\\');
% imagePath               = strcat(dbPath,'ImageScans\\NoFlash\\');
% imagePathFlash          = strcat(dbPath,'ImageScans\\Flash\\');
%
%
% frictionPathFSR1        = strcat(dbPath,'FricScans\\FSR1\\');
% frictionPathFSR2        = strcat(dbPath,'FricScans\\FSR2\\');
%
%
% reflectancePath         = strcat(dbPath,'ReflectanceScans\\');
%
% metalPathTraining       = strcat(dbPath,'MetalDetectionScans\\Training\\');
% metalPathTesting        = strcat(dbPath,'MetalDetectionScans\\Testing\\');
%
%
% %% get files in directories
%
% accelFiles              = dir(strcat(accelPath,'*.txt'));
% accelTapFiles           = dir(strcat(accelTapPath,'*.txt'));
%
% soundFiles              = dir(strcat(soundPath,'*.wav'));    %[dir(strcat(soundPath,'*.wma')); dir(strcat(soundPath,'*.wav')) ];
% soundTapFiles           = dir(strcat(soundTapPath,'*.wav'));
%
% imageFiles              = [dir(strcat(imagePath,'*.jpg')); dir(strcat(imagePath,'*.png')) ];
% imageFilesFlash         = [dir(strcat(imagePathFlash,'*.jpg')); dir(strcat(imagePathFlash,'*.png')) ];
%
% frictionFiles1          = dir(strcat(frictionPathFSR1,'*.txt'));
% frictionFiles2          = dir(strcat(frictionPathFSR2,'*.txt'));
%
%
% reflectanceFiles        = dir(strcat(reflectancePath,'*.txt'));
%
% metalFilesTraining      = dir(strcat(metalPathTraining,'*.txt'));
% metalFilesTesting       = dir(strcat(metalPathTesting,'*.txt'));
%
%
%
% %%%% todo   numQueries 10 training and 5 testing
%
%
%
%
% %% Load Acceleration Data
% if loadCBAR == 1
%     perc = 0;
%
%     labels = Y108;
%
%     surfaceCounter = 0;
%     for i=1:numel(accelFiles)
%         clc
%         disp(strcat(num2str(perc), ' % accel data loaded'));
%         str = strcat(accelPath,accelFiles(i).name);
%         strTrain = strcat(accelPath,'Training\\',accelFiles(i).name);
%         strTest = strcat(accelPath,'Testing\\',accelFiles(i).name);
%
%         if mod(i,3) == 1
%             surfaceCounter = surfaceCounter+1;
%         end
%
%         data = load(str);
%         data = [data;data(100001:155100)];
%
%
%
%         accelFrame = floor( (numel(data)-25000)/10);
%         windowShift = round(accelFrame/2);
%
%         % X Axis
%         if mod(i,3) == 1
%
%             for m = 1:10
%                 aBegin = (m-1) * accelFrame + 1;
%                 aEnd   = m * accelFrame;
%
%                 dbCBAR(10*(surfaceCounter-1)+m).X.dataTraining = data(aBegin:aEnd);
%                 dbCBAR(10*(surfaceCounter-1)+m).X.dataTesting  = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%                 dbCBAR(10*(surfaceCounter-1)+m).X.nameTraining = strcat('TrainingData_',accelFiles(i).name);
%                 dbCBAR(10*(surfaceCounter-1)+m).X.nameTesting  = strcat('TestingData_',accelFiles(i).name);
%
%
%
%                 if(saveIt > 1)
%                     t1 = data(aBegin:aEnd);
%                     t2 = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%                     save(strcat(strTrain(1:end-4),'_train',num2str(m),'.txt'),'t1','-ASCII');
%                     save(strcat(strTest(1:end-4),'_test',num2str(m),'.txt'),'t2','-ASCII');
%                 end
%
%             end
%         end
%
%         % Y Axis
%         if mod(i,3) == 2
%
%             for m = 1:10
%                 aBegin = (m-1) * accelFrame + 1;
%                 aEnd   = m * accelFrame;
%
%                 dbCBAR(10*(surfaceCounter-1)+m).Y.dataTraining = data(aBegin:aEnd);
%                 dbCBAR(10*(surfaceCounter-1)+m).Y.dataTesting  = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%                 dbCBAR(10*(surfaceCounter-1)+m).Y.nameTraining = strcat('TrainingData_',accelFiles(i).name);
%                 dbCBAR(10*(surfaceCounter-1)+m).Y.nameTesting  = strcat('TestingData_',accelFiles(i).name);
%
%                 if(saveIt > 1)
%                     t1 = data(aBegin:aEnd);
%                     t2 = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%                     save(strcat(strTrain(1:end-4),'_train',num2str(m),'.txt'),'t1','-ASCII');
%                     save(strcat(strTest(1:end-4),'_test',num2str(m),'.txt'),'t2','-ASCII');
%                 end
%             end
%         end
%
%         % Z Axis
%         if mod(i,3) == 0
%
%             for m = 1:10
%                 aBegin = (m-1) * accelFrame + 1;
%                 aEnd   = m * accelFrame;
%
%                 dbCBAR(10*(surfaceCounter-1)+m).Z.dataTraining = data(aBegin:aEnd);
%                 dbCBAR(10*(surfaceCounter-1)+m).Z.dataTesting  = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%                 dbCBAR(10*(surfaceCounter-1)+m).Z.nameTraining = strcat('TrainingData_',accelFiles(i).name);
%                 dbCBAR(10*(surfaceCounter-1)+m).Z.nameTesting  = strcat('TestingData_',accelFiles(i).name);
%
%                 if(saveIt > 1)
%                     t1 = data(aBegin:aEnd);
%                     t2 = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%                     save(strcat(strTrain(1:end-4),'_train',num2str(m),'.txt'),'t1','-ASCII');
%                     save(strcat(strTest(1:end-4),'_test',num2str(m),'.txt'),'t2','-ASCII');
%                 end
%             end
%         end
%
%
%
%
%
%
%
%         perc = i*100/numel(accelFiles);
%     end
%
%     for k=1:numQueries * numTextures
%         [dbCBAR(k).dft321.dataTraining,~] = DFT321( dbCBAR(k).X.dataTraining, dbCBAR(k).Y.dataTraining, dbCBAR(k).Z.dataTraining, 1);
%         dbCBAR(k).dft321.nameTraining = labels{k};
%         [dbCBAR(k).dft321.dataTesting,~] = DFT321( dbCBAR(k).X.dataTesting, dbCBAR(k).Y.dataTesting, dbCBAR(k).Z.dataTesting, 1);
%         dbCBAR(k).dft321.nameTesting = labels{k};
%
%         % only uncomment to save RAM
%         %         dbCBAR(k).X = [];
%         %         dbCBAR(k).Y = [];
%         %         dbCBAR(k).Z = [];
%
%     end
%
%
%
%
%
%
%     %     for k=1:numQueries:numInstances
%     %        audiowrite(strcat('../ArtificialSurfaceSynthesis/SynthesizedSignals/',labels{k},'.wav'),dbCBAR(k).dft321.dataTraining,5000);
%     %        audiowrite(strcat('../ArtificialSurfaceSynthesis/SynthesizedSignals/',labels{k},'_10000Hz.wav'),dbCBAR(k).dft321.dataTraining,5000);
%     %        audiowrite(strcat('../ArtificialSurfaceSynthesis/SynthesizedSignals/',labels{k},'_norm.wav'),dbCBAR(k).dft321.dataTraining./2,5000);
%     %        audiowrite(strcat('../ArtificialSurfaceSynthesis/SynthesizedSignals/',labels{k},'_10000Hz_norm.wav'),dbCBAR(k).dft321.dataTraining./2,10000);
%     %        audiowrite(strcat('../ArtificialSurfaceSynthesis/SynthesizedSignals/',labels{k},'_Z.wav'),dbCBAR(k).Z.dataTraining,5000);
%     %        audiowrite(strcat('../ArtificialSurfaceSynthesis/SynthesizedSignals/',labels{k},'_2500Hz.wav'),dbCBAR(k).dft321.dataTraining,2500);
%     %     end
% end
%
% if loadCBAR_TAPPING == 1 && loadCBAR == 1
%     perc = 0;
%     zCounter = 0;
%     %% load accel data
%     for i=1:numel(accelTapFiles)
%         clc;
%         disp(strcat(num2str(perc), ' % accel data loaded'));
%         str = strcat(accelTapPath,accelTapFiles(i).name);
%         strTrain = strcat(accelTapPath,'Training\\',accelTapFiles(i).name);
%         strTest = strcat(accelTapPath,'Testing\\',accelTapFiles(i).name);
%
%         if (isempty(strfind( str,'_Z' )) == 0)
%             data = load(str);
%
%             % todo training test seperation
%             blockSize = floor(numel(data) / 10);
%             for l = 1:10
%                 tmpData = data( ((l-1)*blockSize)+1 : (l*blockSize) );
%                 [~,maxInd] = sort(abs(tmpData(300:end-1600)),'descend');
%                 if maxInd(2) < 100
%                     maxInd(2) = 101;
%                 end
%
%                 dbCBARTAP(10*zCounter+l).dataTraining = tmpData(maxInd(2)-100 : maxInd(2) +1500 );
%                 dbCBARTAP(10*zCounter+l).dataTesting = (1+0.1*randn(1,1)).*tmpData(maxInd(2)-90 : maxInd(2) +1490 );
%                 dbCBARTAP(10*zCounter+l).nameTraining = strcat(strtok(accelTapFiles(i).name,'_'),'_','Tapping_',num2str(l));
%                 dbCBARTAP(10*zCounter+l).nameTesting = strcat(strtok(accelTapFiles(i).name,'_'),'_','Tapping_',num2str(l));
%                 %dbCBARTAP(10*zCounter+l).type = strtok(dbCBAR(i).name,'_');
%
%                 saveStr = strcat(accelTapPathSingleTap, dbCBARTAP(10*zCounter+l).nameTraining);
%                 tmp = dbCBARTAP(10*zCounter+l).dataTraining;
%                 save(saveStr, 'tmp');
%                 tmp1 = dbCBARTAP(10*zCounter+l).dataTraining;
%                 tmp2 = dbCBARTAP(10*zCounter+l).dataTesting;
%
%                 if(saveIt > 1)
%
%                     save(strcat(strTrain(1:end-4),'_train',num2str(l),'.txt'),'tmp1','-ASCII');
%                     save(strcat(strTest(1:end-4),'_test',num2str(l),'.txt'),'tmp2','-ASCII');
%                 end
%
%             end
%             zCounter = zCounter +1;
%         end
%         perc = i*100/numel(accelTapFiles);
%     end
% end
%
%
%
%
% %% Load Sound Data
% dbAudio = {};
% if loadCBSR == 1
%
%     perc = 0;
%     for i = 1:numel(soundFiles)
%         clc
%         disp(strcat(num2str(perc), ' % sound data loaded'));
%         %strcat(soundPath,soundFilesWMA(i).name)
%         %[signal,FS] = audioread(strcat(soundPath,soundFiles(i).name),[1*CBSR_SAMPLERATE 17*CBSR_SAMPLERATE]);
%         [signal,FS] = audioread(strcat(soundPath,soundFiles(i).name));
%         strTrain = strcat(soundPath,'Training\\',soundFiles(i).name);
%         strTest = strcat(soundPath,'Testing\\',soundFiles(i).name);
%         dbAudio{i}.data = signal;
%
%         %info = audioinfo(strcat(soundPath,soundFilesWMA(i).name))
%         signal = signal(:,1);
%         signal = [signal;signal(100001:342991)];
%         soundFrame = floor( (numel(signal))/10);
%         windowShift = round(soundFrame/2);
%         soundFrame = soundFrame - ceil(windowShift/10);
%
%         for m = 1:10
%             aBegin = (m-1) * soundFrame + 1;
%             aEnd   = m * soundFrame;
%
%             dbCBSR(10*(i-1)+m).dataTraining = signal(aBegin:aEnd);
%             dbCBSR(10*(i-1)+m).dataTesting  = (1+0.1*randn(1,1)).*signal(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%             dbCBSR(10*(i-1)+m).nameTraining = strcat('TrainingData_',soundFiles(i).name);
%             dbCBSR(10*(i-1)+m).nameTesting  = strcat('TestingData_',soundFiles(i).name);
%             %dbCBAR(10*(i-1)+m).type = strtok(dbCBAR(i).name,'_');
%             if(saveIt > 1)
%                 t1 = signal(aBegin:aEnd);
%                 t2 = (1+0.1*randn(1,1)).*signal(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%                 audiowrite(strcat(strTrain(1:end-4),'_train',num2str(m),'.wav'),t1,44100);
%                 audiowrite(strcat(strTest(1:end-4),'_test',num2str(m),'.wav'),t2,44100);
%             end
%         end
%
%         %dbCBSR(i).data   = signal(:,1);
%         %dbCBSR(i).name   = soundFiles(i).name;
%
%
%         perc = i*100/numel(soundFiles);
%
%     end
%
% end
% if loadCBSR_TAPPING == 1 && loadCBSR == 1
%     perc = 0;
%     zCounter = 0;
%
%     %% load sound data
%     for i=1:numel(soundTapFiles)
%         clc;
%         disp(strcat(num2str(perc), ' % sound data loaded'));
%
%         [signal,FS] = audioread(strcat(soundTapPath,soundTapFiles(i).name));
%         strTrain = strcat(soundTapPath,'Training\\',soundTapFiles(i).name);
%         strTest = strcat(soundTapPath,'Testing\\',soundTapFiles(i).name);
%         perc = i*100/numel(soundTapFiles);
%
%
%
%         %dbCBSRTAP(i).name   = soundTapFiles(i).name;
%
%         blockSize = floor(numel(signal) / 25);
%
%         for l = 1:10
%
%             tmpData = signal( ((l-1)*blockSize)+1 : (l*blockSize) );
%
%             [~,maxInd] = sort(abs(tmpData(1001:end-10000)),'descend');
%
%             if maxInd(1) < 501
%                 maxInd(1) = 501;
%             end
%
%             %numel(tmpData)
%             %maxInd(1) +10000
%             dbCBSRTAP(10*zCounter+l).dataTraining = tmpData(maxInd(1)-500 : maxInd(1) +10000 )';
%             dbCBSRTAP(10*zCounter+l).nameTraining = strcat(strtok(soundTapFiles(i).name,'_'),'_','Tapping_',num2str(l));
%
%             %dbCBSRTAP(10*zCounter+l).type = strtok(dbCBSRTAP(zCounter+l).name,'_');
%
%             saveStr = strcat(soundTapPathSingleTap,dbCBSRTAP(10*zCounter+l).nameTraining);
%             tmp = dbCBSRTAP(10*zCounter+l).dataTraining;
%             save(strcat(saveStr,'_train'), 'tmp');
%             save(strcat(saveStr,'_train.txt'), 'tmp','-ascii');
%             tmp1 = dbCBSRTAP(10*zCounter+l).dataTraining;
%
%             if(saveIt > 1)
%                 disp('.')
%                 audiowrite(strcat(strTrain(1:end-4),'_train',num2str(l),'.wav'),tmp1,44100);
%             end
%         end
%         for l = 11:20
%
%             tmpData = signal( ((l-1)*blockSize)+1 : (l*blockSize) );
%
%             [~,maxInd] = sort(abs(tmpData(1001:end-10000)),'descend');
%             if maxInd(1) < 501
%                 maxInd(1) = 501;
%             end
%             dbCBSRTAP(10*zCounter+(l-10)).dataTesting = tmpData(maxInd(1)-500 : maxInd(1) +10000 )';
%             dbCBSRTAP(10*zCounter+(l-10)).nameTesting = strcat(strtok(soundTapFiles(i).name,'_'),'_','Tapping_',num2str((l-10)));
%
%             %dbCBSRTAP(10*zCounter+l).type = strtok(dbCBSRTAP(zCounter+l).name,'_');
%
%             saveStr = strcat(soundTapPathSingleTap,dbCBSRTAP(10*zCounter+(l-10)).nameTesting);
%             tmp = dbCBSRTAP(10*zCounter+(l-10)).dataTesting;
%             tmp2 = dbCBSRTAP(10*zCounter+(l-10)).dataTesting;
%             save(strcat(saveStr,'_test'), 'tmp');
%             save(strcat(saveStr,'_test.txt'), 'tmp','-ascii');
%             if(saveIt > 1)
%                 audiowrite(strcat(strTest(1:end-4),'_test',num2str((l-10)),'.wav'),tmp2,44100);
%             end
%         end
%
%
%
%
%         zCounter = zCounter +1;
%
%
%         perc = i*100/numel(soundTapFiles);
%
%
%
%
%     end
% end
%
%
%
%
%
%
%
%
%
%
% %% Load Image Data
% if loadCBIR == 1
%     perc = 0;
%     imgCounter = 1;
%     for i=1:numel(imageFiles)
%         clc;
%         disp(strcat(num2str(perc), ' % image data loaded'));
%         str = strcat(imagePath,imageFiles(i).name);
%         strTrain = strcat(imagePath,'Training\\',imageFiles(i).name);
%         strTest = strcat(imagePath,'Testing\\',imageFiles(i).name);
%
%         if (isempty(strfind( str,'_11' )) == 0) || (isempty(strfind( str,'_12' )) == 0) || (isempty(strfind( str,'_13' )) == 0) || (isempty(strfind( str,'_14' )) == 0) || (isempty(strfind( str,'_15' )) == 0)
%             disp('....')
%         else
%             data = imread(str);
%             %data = data(801:end-801,801:end-801,:);
%             %data = imresize(data,0.7);
%             dbCBIR(imgCounter).noFlash.nameTraining   = imageFiles(i).name;
%             dbCBIR(imgCounter).noFlash.dataTraining   = data(:,1:round(end/2),:);
%             dbCBIR(imgCounter).noFlash.nameTesting    = imageFiles(i).name;
%             dbCBIR(imgCounter).noFlash.dataTesting    = data(:,round(end/2)+1:end,:);
%
%             m= mod(imgCounter,10);
%             imgCounter = imgCounter +1;
%
%             if(saveIt > 1)
%                 t1 = data(:,1:round(end/2),:);
%                 t2 = data(:,round(end/2)+1:end,:);
%                 imwrite(t1,strcat(strTrain(1:end-4),'_train',num2str(m),'.jpg'));
%                 imwrite(t2,strcat(strTest(1:end-4),'_test',num2str(m),'.jpg'));
%             end
%
%         end
%         perc = i*100/numel(imageFiles);
%     end
%
%     imgCounter = 1;
%     for i=1:numel(imageFilesFlash)
%         clc;
%         disp(strcat(num2str(perc), ' % image data loaded'));
%         str = strcat(imagePathFlash,imageFilesFlash(i).name);
%         strTrain = strcat(imagePathFlash,'Training\\',imageFilesFlash(i).name);
%         strTest = strcat(imagePathFlash,'Testing\\',imageFilesFlash(i).name);
%
%         if (isempty(strfind( str,'_11' )) == 0) || (isempty(strfind( str,'_12' )) == 0) || (isempty(strfind( str,'_13' )) == 0) || (isempty(strfind( str,'_14' )) == 0) || (isempty(strfind( str,'_15' )) == 0)
%             disp('')
%         else
%
%             data = imread(str);
%
%             dbCBIR(imgCounter).flash.nameTraining   = imageFilesFlash(i).name;
%             dbCBIR(imgCounter).flash.dataTraining   = data(:,1:round(end/2),:);
%             dbCBIR(imgCounter).flash.nameTesting    = imageFilesFlash(i).name;
%             dbCBIR(imgCounter).flash.dataTesting    = data(:,round(end/2)+1:end,:);
%             m= mod(imgCounter,10);
%             if(saveIt > 1)
%                 t1 = data(:,1:round(end/2),:);
%                 t2 = data(:,round(end/2)+1:end,:);
%                 imwrite(t1,strcat(strTrain(1:end-4),'_train',num2str(m),'.jpg'));
%                 imwrite(t2,strcat(strTest(1:end-4),'_test',num2str(m),'.jpg'));
%             end
%
%             imgCounter = imgCounter +1;
%         end
%         perc = i*100/numel(imageFilesFlash);
%
%     end
% end
%
%
%
%
% %% Load Friction Data
% if loadCBFR == 1
%
%     perc = 0;
%     for i=1:numel(frictionFiles1)
%         clc;
%         disp(strcat(num2str(perc), ' % FSR1 data loaded'));
%         str = strcat(frictionPathFSR1,frictionFiles1(i).name);
%         strTrain = strcat(frictionPathFSR1,'Training\\',frictionFiles1(i).name);
%         strTest = strcat(frictionPathFSR1,'Testing\\',frictionFiles1(i).name);
%         data = load(str);
%         data = [data;data(100001:155100)];
%
%
%
%
%
%
%
%         frictionFrame = floor( (numel(data)-25000)/10);
%         windowShift = round(frictionFrame/2);
%
%         for m = 1:10
%             aBegin = (m-1) * frictionFrame + 1;
%             aEnd   = m * frictionFrame;
%
%             dbCBFR(10*(i-1)+m).FSR1.dataTraining = data(aBegin:aEnd);
%             dbCBFR(10*(i-1)+m).FSR1.dataTesting  = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%             dbCBFR(10*(i-1)+m).FSR1.nameTraining = strcat('TrainingData_',frictionFiles1(i).name);
%             dbCBFR(10*(i-1)+m).FSR1.nameTesting  = strcat('TestingData_',frictionFiles1(i).name);
%
%             if(saveIt > 1)
%                 t1 = data(aBegin:aEnd);
%                 t2 = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%                 save(strcat(strTrain(1:end-4),'_train',num2str(m),'.txt'),'t1','-ASCII');
%                 save(strcat(strTest(1:end-4),'_test',num2str(m),'.txt'),'t2','-ASCII');
%             end
%
%         end
%
%
%
%
%
%
%
%
%
%         %         n = numel(data);
%         %         frame = floor(n/numQueries);
%         %         m = 0;
%         %         for k=1:frame:n-frame
%         %
%         %             dbCBFR( (i*numQueries)-numQueries + m + 1).FSR1.data   = data(k:k+frame);
%         %             dbCBFR( (i*numQueries)-numQueries + m + 1).FSR1.name   = strcat(frictionFiles1(i).name,'_',num2str(m));
%         %             m = m+1;
%         %         end
%         perc = i*100/numel(frictionFiles1);
%     end
%
%     for i=1:numel(frictionFiles2)
%         clc;
%         disp(strcat(num2str(perc), ' % FSR2 data loaded'));
%         str = strcat(frictionPathFSR2,frictionFiles2(i).name);
%         strTrain = strcat(frictionPathFSR2,'Training\\',frictionFiles2(i).name);
%         strTest = strcat(frictionPathFSR2,'Testing\\',frictionFiles2(i).name);
%         data = load(str);
%         data = [data;data(100001:155100)];
%
%         frictionFrame = floor( (numel(data)-25000)/10);
%         windowShift = round(frictionFrame/2);
%
%         for m = 1:10
%             aBegin = (m-1) * frictionFrame + 1;
%             aEnd   = m * frictionFrame;
%
%             dbCBFR(10*(i-1)+m).FSR2.dataTraining = data(aBegin:aEnd);
%             dbCBFR(10*(i-1)+m).FSR2.dataTesting  = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%             dbCBFR(10*(i-1)+m).FSR2.nameTraining = strcat('TrainingData_',frictionFiles2(i).name);
%             dbCBFR(10*(i-1)+m).FSR2.nameTesting  = strcat('TestingData_',frictionFiles2(i).name);
%
%             if(saveIt > 1)
%                 t1 = data(aBegin:aEnd);
%                 t2 = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%                 save(strcat(strTrain(1:end-4),'_train',num2str(m),'.txt'),'t1','-ASCII');
%                 save(strcat(strTest(1:end-4),'_test',num2str(m),'.txt'),'t2','-ASCII');
%             end
%
%         end
%
%
%
%
%
%         %         n = numel(data);
%         %         frame = floor(n/numQueries);
%         %         m = 0;
%         %         for k=1:frame:n-frame
%         %
%         %             dbCBFR( (i*numQueries)-numQueries + m + 1).FSR2.data   = data(k:k+frame);
%         %             dbCBFR( (i*numQueries)-numQueries + m + 1).FSR2.name   = strcat(frictionFiles2(i).name,'_',num2str(m));
%         %             m = m+1;
%         %         end
%         perc = i*100/numel(frictionFiles2);
%     end
% end
%
%
%
%
%
%
%
%
%
%
%
% %% Load Reflectance Data
% if loadCBRR == 1
%
%     perc = 0;
%     for i=1:numel(reflectanceFiles)
%         clc;
%         disp(strcat(num2str(perc), ' % reflectance data loaded'));
%         str = strcat(reflectancePath,reflectanceFiles(i).name);
%         strTrain = strcat(reflectancePath,'Training\\',reflectanceFiles(i).name);
%         strTest = strcat(reflectancePath,'Testing\\',reflectanceFiles(i).name);
%         %strTrainMetal = strcat(metalPath,'Training\\',reflectanceFiles(i).name);
%         %strTestMetal = strcat(metalPath,'Testing\\',reflectanceFiles(i).name);
%         data = load(str);
%         data = [data;data(100001:155100)];
%
%
%
%
%
%         reflectanceFrame = floor( (numel(data)-25000)/10);
%         windowShift = round(reflectanceFrame/2);
%
%         for m = 1:10
%             aBegin = (m-1) * reflectanceFrame + 1;
%             aEnd   = m * reflectanceFrame;
%
%             dbCBRR(10*(i-1)+m).dataTraining = data(aBegin:aEnd);
%             dbCBRR(10*(i-1)+m).dataTesting  = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%             dbCBRR(10*(i-1)+m).nameTraining = strcat('TrainingData_',reflectanceFiles(i).name);
%             dbCBRR(10*(i-1)+m).nameTesting  = strcat('TestingData_',reflectanceFiles(i).name);
%
%
%
%             %                 if(saveIt > 1)
%             %                     t1 = data(aBegin:aEnd);
%             %                     t2 = (1+0.1*randn(1,1)).*data(aBegin+windowShift:aEnd+windowShift) + (0.02*randn(numel(aBegin+windowShift:aEnd+windowShift),1));
%             %                     save(strcat(strTrain(1:end-4),'_train',num2str(m),'.txt'),'t1','-ASCII');
%             %                     save(strcat(strTest(1:end-4),'_test',num2str(m),'.txt'),'t2','-ASCII');
%             %
%             %                     save(strcat(strTrainMetal(1:end-4),'_train',num2str(m),'.txt'),'metalData','-ASCII');
%             %                     save(strcat(strTestMetal(1:end-4),'_test',num2str(m),'.txt'),'metalData','-ASCII');
%             %
%             %                 end
%
%         end
%
%         %         n = numel(data);
%         %         frame = floor(n/numQueries);
%         %         m = 0;
%         %         for k=1:frame:n-frame
%         %
%         %             dbCBRR( (i*numQueries)-numQueries + m + 1).data   = data(k:k+frame);
%         %             dbCBRR( (i*numQueries)-numQueries + m + 1).name   = strcat(reflectanceFiles(i).name,'_',num2str(m));
%         %             m = m+1;
%         %         end
%         perc = i*100/numel(reflectanceFiles);
%     end
%
%
% end
%
%
% %% Load Metal Data
% if loadCBMR == 1
%
%     perc = 0;
%     numel(metalFilesTraining)
%     for i=1:numel(metalFilesTraining)
%         clc;
%         disp(strcat(num2str(perc), ' % Metal data loaded'));
%         str = strcat(metalPathTraining,metalFilesTraining(i).name);
%
%         data = load(str);
%
%
%         dbCBMR(i).dataTraining = data;
%         dbCBMR(i).nameTraining = metalFilesTraining(i).name;
%
%
%         perc = i*100/numel(metalFilesTraining);
%     end
%     perc = 0;
%     for i=1:numel(metalFilesTesting)
%         clc;
%         disp(strcat(num2str(perc), ' % Metal data loaded'));
%         str = strcat(metalPathTesting,metalFilesTesting(i).name);
%
%         data = load(str);
%
%
%         dbCBMR(i).dataTesting = data;
%         dbCBMR(i).nameTesting = metalFilesTesting(i).name;
%
%
%         perc = i*100/numel(metalFilesTesting);
%     end
%
%
% end
%
%

