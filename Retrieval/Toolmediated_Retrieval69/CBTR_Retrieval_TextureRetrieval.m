function [ similarityStruct,textureMatrix ] = CBTR_Retrieval_TextureRetrieval( displayIt )
%TEXTURERETRIEVAL Summary of this function goes here
%   Detailed explanation goes here

    similarityStruct = struct;
    numTextures = 69;
    numQueries  = 10;
    numSubjects = 10;
    outlierRemove = 0;
    numOfSimTex = 5;
    textureMatrix = zeros(numTextures,numTextures);
	
    
    M = xlsread('CBTR_Retrieval_RetrievalSelection.xlsx');

    labels = load('CBTR_Retrieval_labelNames.mat');
    labels = labels.labelNames;
    labels = labels(numQueries*[1:numTextures]);

    
%     labelMapper = xlsread('CBTR_Retrieval_labelMapping.xlsx');
%     [sortedLabelsNum,sortedLabelsIndices] = sort(labelMapper(:,2),'ascend');
%     l = labelMapper(:,1);
%     sortedLabels = l(sortedLabelsIndices);
%     labels = labels(numQueries*sortedLabels)
%     save('CBTR_Retrieval_retrievalExpLabelNames.mat','labels'); 

    
    
    
    % for all columns ( = subjects) ...
    for subject = 1:numSubjects
        data = M(:,subject);
        amountOfGroups = max(data);

        for k=1:numTextures
            currentTextureGroup = data(k,1);

            for j=1:numTextures
                if k==j
                    continue;
                end
          
                textureGroup = data(j,1);
                if textureGroup == currentTextureGroup
                    textureMatrix(k,j) = textureMatrix(k,j) + 1;
                end
            end
        end
        
    end

   
    
    % sorting weights section and defining similar textures
    for k=1:numTextures
        
        textureNumbers = [];
        textureWeights = [];
        
        for j=1:numTextures    
            if textureMatrix(k,j) > 0       
                textureNumbers = [textureNumbers; j];
                textureWeights = [textureWeights; textureMatrix(k,j)];         
            end 
        end
    
        [sortedTextureWeightsValues,sortedTextureWeightsIndices] = sort(textureWeights,'descend');
        similarityStruct(k).ownName         = labels(k);
        similarityStruct(k).numbers         = textureNumbers(sortedTextureWeightsIndices);
        similarityStruct(k).weights         = sortedTextureWeightsValues;
        similarityStruct(k).names           = labels(similarityStruct(k).numbers);
        similarityStruct(k).similar         = struct;
        %similarityStruct(k).fiveSimilar     = similarityStruct(k).names(1:numOfSimTex);
        o = 1;
        maxWeight = max(similarityStruct(k).weights);
        for j=maxWeight:-1:1
            
            indices = find(similarityStruct(k).weights == j);
            if(numel(indices) == 0)
                continue;
            end
            
            similarityStruct(k).similar(o).similar = labels(similarityStruct(k).numbers(indices(1):indices(end)));
            similarityStruct(k).similar(o).amount  =  numel(similarityStruct(k).numbers(indices(1):indices(end)));
            
            o = o + 1;
        end
        similarityStruct(k).groups = o-1;
        
        % most similar texture
        similarityStruct(k).mostSimilar         = similarityStruct(k).similar(1).similar;
        similarityStruct(k).mostSimilarAmount   = numel(similarityStruct(k).similar(1).similar);    
        
        
        
        % three similar textures
        if similarityStruct(k).groups >= 3
            similarityStruct(k).threeMostSimilar = [similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar;similarityStruct(k).similar(3).similar];
            similarityStruct(k).threeMostSimilarAmount = numel([similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar;similarityStruct(k).similar(3).similar]);        
        elseif similarityStruct(k).groups == 2
            similarityStruct(k).threeMostSimilar = [similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar];
            similarityStruct(k).threeMostSimilarAmount = numel([similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar]);   
        elseif similarityStruct(k).groups == 1
            similarityStruct(k).threeMostSimilar = [similarityStruct(k).similar(1).similar];
            similarityStruct(k).threeMostSimilarAmount = numel([similarityStruct(k).similar(1).similar]);    
        else
            error('Grouping error.abort.')
        end      
        
        
        
        
        
        % five similar textures
        if similarityStruct(k).groups >= 5
            similarityStruct(k).fiveMostSimilar = [similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar;similarityStruct(k).similar(3).similar;similarityStruct(k).similar(4).similar;similarityStruct(k).similar(5).similar];
            similarityStruct(k).fiveMostSimilarAmount = numel([similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar;similarityStruct(k).similar(3).similar;similarityStruct(k).similar(4).similar;similarityStruct(k).similar(5).similar]);
        elseif similarityStruct(k).groups == 4
            similarityStruct(k).fiveMostSimilar = [similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar;similarityStruct(k).similar(3).similar;similarityStruct(k).similar(4).similar;' '];
            similarityStruct(k).fiveMostSimilarAmount = numel([similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar;similarityStruct(k).similar(3).similar;similarityStruct(k).similar(4).similar;' ']);         
        elseif similarityStruct(k).groups == 3
            similarityStruct(k).fiveMostSimilar = [similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar;similarityStruct(k).similar(3).similar;' ';' '];
            similarityStruct(k).fiveMostSimilarAmount = numel([similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar;similarityStruct(k).similar(3).similar;' ';' ']);        
        elseif similarityStruct(k).groups == 2
            similarityStruct(k).fiveMostSimilar = [similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar;' ';' ';' '];
            similarityStruct(k).fiveMostSimilarAmount = numel([similarityStruct(k).similar(1).similar;similarityStruct(k).similar(2).similar;' ';' ';' ']);   
        elseif similarityStruct(k).groups == 1
            similarityStruct(k).fiveMostSimilar = [similarityStruct(k).similar(1).similar;' ';' ';' ';' '];
            similarityStruct(k).fiveMostSimilarAmount = numel([similarityStruct(k).similar(1).similar;' ';' ';' ';' ']);    
        else
            error('Grouping error.abort.')
        end
        
    end
    
    
 
    
    % outlier removal for displaying
    if outlierRemove == 1
        for k=1:numTextures     
            for j=1:numTextures    
                if textureMatrix(k,j) == 1
                    textureMatrix(k,j) = 0;
                end
            end
        end
    end
    
    % displaying 
    if displayIt == 1  
        for k = 1:numTextures
            disp(' ')
            disp('_______________________________________')
            disp(' ')
            disp(strcat(' ' ,num2str(k),'...',labels(k),' is similar to...'));
            for j = 1:numel(similarityStruct(k).numbers)
                disp(strcat('.........' ,  labels(similarityStruct(k).numbers(j)) ,'...with weight...',num2str(similarityStruct(k).weights(j)) ));
            end
            disp(' ')

        end
        
    
        figure;
        imagesc(textureMatrix); %title(strcat('Naive BayesClassification   Accuracy:',num2str(accuracy),'%'));
        colormap(1-gray);
        gray;
        colorbar;
     
        
        set(gca(),'YTick',[1:numTextures])
        set(gca(),'YTickLabel' ,labels,'FontName','Times','fontsize',8); 
        set(gcf, 'Position', [0, 0, 1324, 1068]); 
        xticklabel_rotate([1:numTextures],60,labels,'interpreter','none','FontName','Times','fontsize',8)

        set(gcf,'PaperUnits','centimeters');
        set(gcf,'PaperSize',[27.0 27.0]);
        %set(gcf,'PaperSize',[13.0 12.0]);
        set(gcf,'PaperPosition',[1.0,-1.0,28,28.0]);
        set(gcf,'PaperPositionMode','manual');
        print(gcf,'-dpdf','TextureSimilarityMatrix');        
       
        
        
        
        
%         %http://de.mathworks.com/help/stats/multidimensional-scaling.html
%         [Y,eigvals] = cmdscale(textureMatrix);
%         format short g
%         [eigvals eigvals/max(abs(eigvals))]
%         
%         Dtriu = textureMatrix(find(tril(ones(numTextures),-1)))';
%         maxrelerr = max(abs(Dtriu-pdist(Y(:,1:2))))./max(Dtriu)
%         
%         
%         figure;plot3(Y(:,3),Y(:,4),Y(:,7),'.')
%         text(Y(:,3)+5,Y(:,4),Y(:,7),labels)

        
    end    
    
 
    
    
end

