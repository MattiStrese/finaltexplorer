function [ finalFeatures ] = CBTR_Retrieval_FindRecallFeatures( mode )



if (strcmp(mode,'forward') == 1)

    for q=28:30%30

        finalFeatures = [q];
        oldRecall = 0;
        for j=1:30
            
            list = [];
            for k = 1:30

                skipFeature = 0;
                for m = 1:numel(finalFeatures)
                    if k == finalFeatures(m)
                       skipFeature = 1; 
                       b = 0;
                       list = [list,0];
                    end
                end
                if skipFeature == 0

                    [~,b] = CBTR_MAIN_Retrieval([finalFeatures,k],0,0,'Distance','Discriminant',0);
                    disp(strcat('Feature__',num2str(k),'__________recall__________________',num2str(b)));
                    list = [list,b];



                end


            end



            [~,bestFeature] = max(list);

            if max(list) < oldRecall
                break;
            else
                oldRecall = max(list);
            end

            bestFeature

            finalFeatures = [finalFeatures,bestFeature]

        end

        disp('_____________________')
        disp(' ')

        recallValue = max(list);
        finalFeatures

        strFinalFeatures = strcat('RetrievalFeatureSelection/retrievalFeatures',num2str(q),'.mat')
        strFinalRecall = strcat('RetrievalFeatureSelection/retrievalRecall',num2str(q),'.mat')
        save(strFinalFeatures,'finalFeatures');
        save(strFinalRecall,'recallValue');
    end

    
    
    
    
    
    
    
    
    
    
elseif (strcmp(mode,'backward') == 1)
    
    finalFeatures = [1:30];
    deletedList = [];
    
    recallCurve = [];
    
    oldRecall = 0;
    for j=1:30
        
        currFeatures = finalFeatures;
        
        list = [];
        for k = 1:30
            
            if (numel( deletedList(deletedList == k) ) > 0)
                disp(strcat('skip feature...',num2str(k),'...(already excluded from list'));
                list = [list,1000000];
                continue;
            end
            
            tmp = currFeatures(currFeatures~=k);
            [~,b] = CBTR_MAIN_Retrieval(tmp,0,0,'Distance','Discriminant',0);
            disp(strcat('Feature__',num2str(k),'__________recall__________________',num2str(b)));
            list = [list,b];
            
            
            
            
            
        end
        
        
        
        
        
        
        [~,worstFeature] = min(list);
        
%         if min(list) < oldRecall
%             break;
%         else
%             oldRecall = min(list);
%         end
   

        worstFeature
        
        finalFeatures = currFeatures(currFeatures~=worstFeature);
       
        [~,tmp2] = CBTR_MAIN_Retrieval(finalFeatures,0,0,'Distance','Discriminant',0);
        recallCurve = [recallCurve,tmp2]
        
        finalFeatures
        
        deletedList = [deletedList,worstFeature];
    end
    
    disp('_____________________')
    disp(' ')
    
    recallValue = max(list);
    finalFeatures
    
    strFinalFeatures = strcat('RetrievalFeatureSelection/BACKWARDretrievalFeatures',num2str(1),'.mat')
    strFinalRecall = strcat('RetrievalFeatureSelection/BACKWARDretrievalRecall',num2str(1),'.mat')
    save(strFinalFeatures,'finalFeatures');
    save(strFinalRecall,'recallCurve');

    figure;stem(recallCurve)
    
    


else
    error('Wrong mode entered.Abort.')
end
    
end

