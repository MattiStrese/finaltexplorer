function [ plainFile,plainFile_SubjTest ] = CBTR_SubjTest_Training(  )
%   Description:        
%       This function calculates the training and test data sets for
%       further Machine Learning processing and evaluation
%
%   Input:  acceleration struct 
%   Output: 
%           ...
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................



    % Ensure correct number of inputs
    if( nargin~=  0), help CBTR_SubjTest_Training; 
        return; 
    end; 

    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
       CBTR_Constants 
    end

    
    % local variables
    plainFile           = [];
    plainFile_SubjTest  = [];

    
    p1 = load('MatlabData/CBAR_PlainFile.mat');
    p2 = load('MatlabData/CBSR_PlainFile.mat');
    p3 = load('MatlabData/CBIR_plainFileMagnified.mat');
    p4 = load('MatlabData/CBFR_PlainFile.mat');
    p1 = p1.CBAR_PlainFile;
    p2 = p2.CBSR_PlainFile;
    p3 = p3.Feature_Matrix(:,1:9);
    p4 = p4.CBFR_PlainFile;
    
    p5 = load('MatlabData/CBAR_SubjTest_PlainFile.mat');
    p6 = load('MatlabData/CBSR_SubjTest_PlainFile.mat');
    p7 = load('MatlabData/CBIR_plainFileMagnified.mat');
    p8 = load('MatlabData/CBFR_SubjTest_PlainFile.mat');
    p5 = p5.CBAR_SubjTest_PlainFile;
    p6 = p6.CBSR_SubjTest_PlainFile;
    p7 = p7.Feature_Matrix(:,1:9);
    p8 = p8.CBFR_SubjTest_PlainFile;
    
    p9 = load('MatlabData/CBAR_SubjTest_PlainFileWithoutNormalisation.mat');
    p10 = load('MatlabData/CBSR_SubjTest_PlainFileWithoutNormalisation.mat');
    % TODO still using normalized values for cbir
    p11 = load('MatlabData/CBIR_plainFileMagnified.mat');
    p12 = load('MatlabData/CBFR_SubjTest_PlainFileWithoutNormalisation.mat');
    p9 = p9.CBAR_SubjTest_PlainFile;
    p10 = p10.CBSR_SubjTest_PlainFile;
    p11 = p11.Feature_Matrix(:,1:9);
    p12 = p12.CBFR_SubjTest_PlainFile;   
    
    disp(strcat('Number of accel features:___',num2str(numel(p1(1,:)))));
    disp(strcat('Number of sound features:___',num2str(numel(p2(1,:)))));
    disp(strcat('Number of image features:___',num2str(numel(p3(1,:)))));
    disp(strcat('Number of frict features:___',num2str(numel(p4(1,:)))));
    
    
    plainFile = [ p1 p2 p3 p4 ];
    plainFile_SubjTest = [p5 p6 p7 p8];
    plainFile_SubjTestWithoutNormalisation = [p9 p10 p11 p12];
    
    save 'MatlabData/plainFile.mat' plainFile 
    save 'MatlabData/plainFile_SubjTest.mat' plainFile_SubjTest 
    save 'MatlabData/plainFile_SubjTestWithoutNormalisation.mat' plainFile_SubjTestWithoutNormalisation 
    
    %% create training and test data (10fold cross validation)
    for k = 1:numQueries
        dataTesting  = plainFile_SubjTest(k:numQueries:numInstances,:);
        dataTraining = plainFile;
        
        
        plainFileCombined = plainFile;
        plainFileCombined(k:numQueries:numInstances,:) = dataTesting;
        %dataTraining(k:numQueries:numInstances,:) = [];
        
        % save each combination of test and training data in seperate
        % matlab files
        strTrain = strcat('TrainingTestingDataForSubjTest/dataTraining',num2str(k),'.mat');
        strTest  = strcat('TrainingTestingDataForSubjTest/dataTesting',num2str(k),'.mat');    
        save(strTrain,'dataTraining');
        save(strTest, 'dataTesting');
        
        
        
        strCombined = strcat('MatlabData/plainFileCombined',num2str(k),'.mat');
        save(strCombined,'plainFileCombined');

        
    end
    
    
    
end

