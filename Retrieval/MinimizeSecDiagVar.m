function [ Matrix ] = MinimizeSecDiagVar( Matrix, Iterations )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

for ii = 1:Iterations
    fprintf('Iteration %i\n', ii)
    jj = round(rand()*(size(Matrix,1) - 1) + 1);
    kk = round(rand()*(size(Matrix,1) - 1) + 1);
        buffer = (linspace(1,(size(Matrix, 1) - jj + 1),(size(Matrix, 1) - jj + 1)));
        var = sum((Matrix(jj,jj:size(Matrix))).*buffer);
        var2 = sum((Matrix(kk,jj:size(Matrix))).*buffer);
        buffer = (linspace(jj, 1, jj));
        var = var + sum((Matrix(jj,1:jj)).*buffer);
        var2 = var2 + sum((Matrix(kk,1:jj)).*buffer);
        var = var/sum(Matrix(jj,:));
        var2 = var2/sum(Matrix(kk,:));
        if var2 < var
            a = Matrix(jj,:);
            Matrix(jj,:) = Matrix(kk,:);
            Matrix(kk,:) = a;
            a = Matrix(:,jj);
            Matrix(:,jj) = Matrix(:,kk);
            Matrix(:,kk) = a;    
        end;
end;
        

end

