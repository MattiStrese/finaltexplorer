function [ dbCBAR,dbCBARTAP,dbCBSR,dbCBSRTAP,dbCBIR,dbCBFR ] = CBTR_SubjTest_LoadFromDatabase( loadCBAR, loadCBAR_TAPPING, loadCBSR, loadCBSR_TAPPING, loadCBIR, loadCBFR)
%   Description:
%       This function loads all queries into a struct
%
%   Input:  none
%   Output:
%           accel data struct
%           audio data struct
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................


% Ensure correct number of inputs
if( nargin~=  6), help CBTR_SubjTest_LoadFromDatabase;
    return;
end;

% Check, if CBTR_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    CBTR_Constants
end


%%
%  output
dbCBAR      = struct;
dbCBARTAP   = struct;
dbCBSR      = struct;
dbCBSRTAP   = struct;
dbCBIR      = struct;
dbCBFR      = struct;







accelPath               = strcat(dbPath,'SubjectiveExploration\\AccelScansDFT321\\');
accelTapPath            = strcat(dbPath,'Tapping69\\AccelScansComponents\\');
soundPath               = strcat(dbPath,'SubjectiveExploration\\SoundScans\\');
soundTapPath            = strcat(dbPath,'Tapping69\\SoundScans\\');
imagePath               = strcat(dbPath,'ImageMagnified\\');
frictionPath            = strcat(dbPath,'SubjectiveExploration\\FricScans\\');
accelTapPathSingleTap   = strcat(dbPath,'SubjectiveExploration\\AccelScansComponents\\FreehandTapping\\');
soundTapPathSingleTap   = strcat(dbPath,'SubjectiveExploration\\SoundScans\\FreehandTapping\\');


%% get files in directories

accelFiles              = dir(strcat(accelPath,'*.txt'));
accelTapFiles           = dir(strcat(accelTapPath,'*.txt'));
soundFiles              = dir(strcat(soundPath,'*.wav'));    %[dir(strcat(soundPath,'*.wma')); dir(strcat(soundPath,'*.wav')) ];
soundTapFiles           = dir(strcat(soundTapPath,'*.wav'));
imageFiles              = [dir(strcat(imagePath,'*.jpg')); dir(strcat(imagePath,'*.png')) ];
frictionFiles           = dir(strcat(frictionPath,'*.txt'));








%% Load Acceleration Data
if loadCBAR == 1
    perc = 0;
    for i=1:numel(accelFiles)
        clc;
        disp(strcat(num2str(perc), ' % accel data loaded'));
        str = strcat(accelPath,accelFiles(i).name);
        data = load(str);
        
        dbCBAR(i).data = data;
        dbCBAR(i).name = accelFiles(i).name;
        dbCBAR(i).type = strtok(dbCBAR(i).name,'_');
        perc = i*100/numel(accelFiles);
    end
end


if loadCBAR_TAPPING == 1 && loadCBAR == 1
    perc = 0;
    zCounter = 0;
    load subjNames.mat
    for i=1:numel(accelTapFiles)
        clc;
        disp(strcat(num2str(perc), ' % accel data loaded'));
        str = strcat(accelTapPath,accelTapFiles(i).name);
        if (isempty(strfind( str,'_Z' )) == 0)
            data = load(str);
            
            
            blockSize = floor(numel(data) / 10);
            for l = 1:10
                tmpData = data( ((l-1)*blockSize)+1 : (l*blockSize) );
                [maxVal,maxInd] = max(abs(tmpData(300:end-1600)));
                if maxInd < 100
                    maxInd = 101;
                end
                
                dbCBARTAP(10*zCounter+l).data = tmpData(maxInd-100 : maxInd +1500 );
                dbCBARTAP(10*zCounter+l).name = strcat(strtok(accelTapFiles(i).name,'_'),'_',subjNames{l});
                dbCBARTAP(10*zCounter+l).type = strtok(dbCBARTAP(zCounter+l).name,'_');
                
                saveStr = strcat(accelTapPathSingleTap, dbCBARTAP(10*zCounter+l).name);
                tmp = dbCBARTAP(10*zCounter+l).data;
                save(saveStr, 'tmp');
                
            end
            zCounter = zCounter +1;
        end
        
        perc = i*100/numel(accelTapFiles);
        
        
        
        
    end
end






%% Load Sound Data
if loadCBSR == 1
    perc = 0;
    for i = 1:numel(soundFiles)
        clc
        disp(strcat(num2str(perc), ' % sound data loaded'));
        %strcat(soundPath,soundFilesWMA(i).name)
        [signal,FS] = audioread(strcat(soundPath,soundFiles(i).name));
        %info = audioinfo(strcat(soundPath,soundFilesWMA(i).name))
        
        
        dbCBSR(i).data   = signal(:,1);
        dbCBSR(i).name   = soundFiles(i).name;
        perc = i*100/numel(soundFiles);
        
    end
end
if loadCBSR_TAPPING == 1 && loadCBSR == 1
    perc = 0;
    zCounter = 0;
    load subjNames.mat
    for i=1:numel(soundTapFiles)
        clc;
        disp(strcat(num2str(perc), ' % sound data loaded'));
        
        [signal,FS] = audioread(strcat(soundTapPath,soundTapFiles(i).name));
        
        
        
        
        dbCBSRTAP(i).name   = soundTapFiles(i).name;
        perc = i*100/numel(soundTapFiles);
        
        
        blockSize = floor(numel(signal) / 10);
        for l = 1:10
            tmpData = signal( ((l-1)*blockSize)+1 : (l*blockSize) );
            [maxVal,maxInd] = max(abs(tmpData(5000:end-21000)));
            if maxInd < 5000
                maxInd = 5001;
            end
            
            dbCBSRTAP(10*zCounter+l).data = tmpData(maxInd-1000 : maxInd +12000 );
            dbCBSRTAP(10*zCounter+l).name = strcat(strtok(soundTapFiles(i).name,'_'),'_',subjNames{l});
            dbCBSRTAP(10*zCounter+l).type = strtok(dbCBSRTAP(zCounter+l).name,'_');
            
            saveStr = strcat(soundTapPathSingleTap,dbCBSRTAP(10*zCounter+l).name);
            tmp = dbCBSRTAP(10*zCounter+l).data;
            save(saveStr, 'tmp');
        end
        zCounter = zCounter +1;
        
        
        perc = i*100/numel(soundTapFiles);
        
        
        
        
    end
end




%% Load Image Data
if loadCBIR == 1
    perc = 0;
    for i=1:numel(imageFiles)
        clc;
        disp(strcat(num2str(perc), ' % image data loaded'));
        str = strcat(imagePath,imageFiles(i).name);
        data = imread(str);
        
        %dbCBIR(i).name   = imageFiles(i).name;
        dbCBIR(i).name   = Y108{i};
        dbCBIR(i).data   = data(501:2000,1201:2700,:);
        perc = i*100/numel(imageFiles);
    end
end








%% Load Friction Data
if loadCBFR == 1
    perc = 0;
    for i=1:numel(frictionFiles)
        clc;
        disp(strcat(num2str(perc), ' % friction data loaded'));
        str = strcat(frictionPath,frictionFiles(i).name);
        data = load(str);
        
        dbCBFR(i).data = data;
        dbCBFR(i).name = frictionFiles(i).name;
        dbCBFR(i).type = strtok(dbCBAR(i).name,'_');
        perc = i*100/numel(frictionFiles);
    end
end

end

