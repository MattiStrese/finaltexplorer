function [ output_args ] = CBTR_MachineLearning( mlMethod, plotIt )
%CBTR_MACHINELEARNING Summary of this function goes here
%   Detailed explanation goes here


% Ensure correct number of inputs
if( nargin~= 2 ), help CBTR_MachineLearning;
    return;
end;




% Check, if CBTR_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    CBTR_Constants
end



if plotIt == 1
    figure;
end



trainingData = featureSpace_Training(:,selectedFeatures); 
testData = featureSpace_Testing(:,selectedFeatures);
X = trainingData;
Y = Y108;

if (strcmp(mlMethod,'NaiveBayes') == 1)
    

    classifier1  = NaiveBayes.fit(X, Y);
    predicted_species1 = predict(classifier1,testData);
    confMat = confusionmat(Y,predicted_species1);
    confMat = confMat';
    
    
    tmp = posterior(classifier1,testData);
    bayesSimilar = zeros(numTextures,numTextures);
    for k=0:numTextures-1
        bayesSimilar(k+1,:) = sum(tmp( (((numQueries*k)+1):((numQueries*k)+numQueries)),:));
        bayesSimilar(k+1,k+1) = 0;
    end
    
    %tmp = max(max(bayesSimilar));
    %bayesSimilar(:,:) = 10.*bayesSimilar(:,:) ./ tmp;
    
    classes = unique(Y);
    if plotIt == 1
        disp('Major confusions:')
        for k=1:numel(classes)
            for l=1:numel(classes)
                if k == l
                    continue
                end
                if confMat(k,l) > 2
                    disp(strcat('Confused: ',classes(k),' with: ', classes(l)   ))
                end
            end
        end
    end
    accuracy = 100 * sum(sum(diag(diag(confMat)))) / sum(sum(confMat));
    %figure;grid off;
    if plotIt == 1      
        %figure;
        imagesc(confMat); title(strcat('Naive BayesClassification   Accuracy:',num2str(accuracy),'%'));
        colormap(1-gray);
        gray;
        colorbar;
        set(gcf,'PaperUnits','centimeters');
        set(gcf,'PaperSize',[13.0 12.0]);
        set(gcf,'PaperPosition',[-0.2,0,14,12.0]);
        set(gcf,'PaperPositionMode','manual');
        %print(gcf,'-dpdf','NaiveBayesConfusionMatrix');
        

    end
    
    
    
    
    
    
    
    
    
    
elseif strcmp(mlMethod,'NaiveBayes2') == 1
    
    % 69 classes
    classes = unique(Y);
    
    Mdl = fitcnb(X,Y,'ClassNames',classes);
    rng(1); % For reproducibility
    defaultCVMdl = crossval(Mdl);
    defaultLoss = kfoldLoss(defaultCVMdl)
    CVMdl = crossval(Mdl);
    Loss = kfoldLoss(CVMdl);
    
    isLabels1 = resubPredict(Mdl);
    ConfusionMat1 = confusionmat(Y,isLabels1);
    
    CBTR_ConfusionMatrix(ConfusionMat1, 'Naive Bayes 2');
    
    if plotIt == 1
        disp('Major confusions:')
        for k=1:numel(classes)
            for l=1:numel(classes)
                if k == l
                    continue
                end
                if ConfusionMat1(k,l) > 1
                    disp(strcat('Confused: ',classes(k),' with: ', classes(l)   ))
                end
            end
        end
    end
    
    
    [dimA,dimB] = size(X);
    if dimB == 2
        numClasses = 69;
        figure
        gscatter(X(:,1),X(:,2),Y);
        h = gca;
        xylim = [h.XLim h.YLim]; % Get current axis limits
        hold on
        Params = cell2mat(Mdl.DistributionParameters);
        Mu = Params(2*(1:numClasses)-1,1:2); % Extract the means
        Sigma = zeros(2,2,numClasses);
        for j = 1:numClasses
            Sigma(:,:,j) = diag(Params(2*j,:)); % Extract the standard deviations
            ezcontour(@(x1,x2)mvnpdf([x1,x2],Mu(j,:),Sigma(:,:,j)),xylim+0.5*[-1,1,-1,1])     % Draw contours for the multivariate normal distributions
        end
        title('Naive Bayes Classifier')
        xlabel('Feature 1')
        ylabel('Feature 2')
        hold off
    end   
    
    
    
elseif strcmp(mlMethod,'NaiveBayes3') == 1
    
    % 69 classes
    classes = unique(Y);
    
    Mdl = fitcnb(trainingData,Y,'ClassNames',classes);
    rng(1); % For reproducibility
    %defaultCVMdl = crossval(Mdl)
    %defaultLoss = kfoldLoss(defaultCVMdl)
    %CVMdl = crossval(Mdl);
    %Loss = kfoldLoss(CVMdl);
    
    %isLabels1 = resubPredict(Mdl);
    isLabels1 = predict(Mdl,testData);
    
    ConfusionMat1 = confusionmat(Y,isLabels1);
    
    CBTR_ConfusionMatrix(ConfusionMat1, 'Naive Bayes 3');
    
    
    disp('Major confusions:')
    for k=1:numel(classes)
        for l=1:numel(classes)
            if k == l
                continue
            end
            if ConfusionMat1(k,l) > 1
                disp(strcat('Confused: ',classes(k),' with: ', classes(l)   ))
            end
        end
    end
    
    
    
    
    
    
    
    
    
    %%
elseif (strcmp(mlMethod,'Discriminant') == 1)
    
    classifier2  = ClassificationDiscriminant.fit(X,Y);
    
    predicted_species2 = predict(classifier2,X);
    confMat = confusionmat(Y,predicted_species2);
    confMat = confMat';
    accuracy = 100 * sum(sum(diag(diag(confMat)))) / sum(sum(confMat));
    %figure;grid off;
    if plotIt == 1
        imagesc(confMat); title(strcat('Discriminant Analysis   Accuracy:',num2str(accuracy),'%'));
        colormap(1-gray);
        gray;
        colorbar;
        set(gcf,'PaperUnits','centimeters');
        set(gcf,'PaperSize',[13.0 12.0]);
        set(gcf,'PaperPosition',[-0.2,0,14,12.0]);
        set(gcf,'PaperPositionMode','manual');
        print(gcf,'-dpdf','DiscriminantAnalysisConfusionMatrix');
    end
    
    
    
    
    
    %%
elseif (strcmp(mlMethod,'Tree') == 1)
    classifier3  = ClassificationTree.fit(X,Y);
    
    predicted_species3 = predict(classifier3,X);
    confMat = confusionmat(Y,predicted_species3);
    confMat = confMat';
    accuracy = 100 * sum(sum(diag(diag(confMat)))) / sum(sum(confMat));
    %figure;grid off;
    if plotIt == 1
        imagesc(confMat); title(strcat('Classification Tree   Accuracy:',num2str(accuracy),'%'));
        colormap(1-gray);
        gray;
        colorbar;
        set(gcf,'PaperUnits','centimeters');
        set(gcf,'PaperSize',[13.0 12.0]);
        set(gcf,'PaperPosition',[-0.2,0,14,12.0]);
        set(gcf,'PaperPositionMode','manual');
        print(gcf,'-dpdf','DecisionTreeConfusionMatrix');
    end
    
    classes = unique(Y);
    disp('Major confusions:')
    for k=1:numel(classes)
        for l=1:numel(classes)
            if k == l
                continue
            end
            if confMat(k,l) > 2
                disp(strcat('Confused: ',classes(k),' with: ', classes(l)   ))
            end
        end
    end
 
elseif strcmp(mlMethod,'Tree2') == 1
    
    classes = unique(Y);
    rng(1); % For reproducibility
    
    t = fitctree(trainingData, Y,'Prune','off');    
    %defaultCVMdl = crossval(t);
  
    Y_hat = predict(t, testData);
    confusionMat = confusionmat(Y,Y_hat);   
    tt = prune(t, 'Level',3);
    %view(tt)

    
    
    CBTR_ConfusionMatrix(confusionMat, 'Tree 2');
    disp('Major confusions:')
    for k=1:numel(classes)
        for l=1:numel(classes)
            if k == l
                continue
            end
            if confusionMat(k,l) > 1
                disp(strcat('Confused: ',classes(k),' with: ', classes(l)   ))
            end
        end
    end
    
    
    
elseif strcmp(mlMethod,'NeuralNetwork') == 1
    
    
    disp('Not implemented yet.')
    
    
    
elseif strcmp(mlMethod,'GMM') == 1
    % http://stackoverflow.com/questions/26019584/understanding-concept-of-gaussian-mixture-models
    [tmp1,tmp2] = size(X);
    X = X+0.00001*randn(tmp1,tmp2);
    
    data = X;
    
mn = min(data); mx = max(data);
D = size(data,2);    % data dimension    

% inital kmeans step used to initialize EM
K = 5;               % number of mixtures/clusters
cInd = kmeans(data, K, 'EmptyAction','singleton');

% fit a GMM model
gmm = fitgmdist(data, K, 'Options',statset('MaxIter',1000), ...
    'CovType','full', 'SharedCov',false, 'Regularize',0.01, 'Start',cInd);

% means, covariances, and mixing-weights
mu = gmm.mu;
sigma = gmm.Sigma;
p = gmm.PComponents;

% cluster and posterior probablity of each instance
% note that: [~,clustIdx] = max(p,[],2)
[clustInd,~,p] = cluster(gmm, data);
tabulate(clustInd)

% plot data, clustering of the entire domain, and the GMM contours
clrLite = [1 0.6 0.6 ; 0.6 1 0.6 ; 0.6 0.6 1];
clrDark = [0.7 0 0 ; 0 0.7 0 ; 0 0 0.7];
[X,Y] = meshgrid(linspace(mn(1),mx(1),50), linspace(mn(2),mx(2),50));
C = cluster(gmm, [X(:) Y(:)]);
image(X(:), Y(:), reshape(C,size(X))), hold on
gscatter(data(:,1), data(:,2))
h = ezcontour(@(x,y)pdf(gmm,[x y]), [mn(1) mx(1) mn(2) mx(2)]);
set(h, 'LineColor','k', 'LineStyle',':')
hold off, axis xy, colormap(clrLite)
title('2D data and fitted GMM'), xlabel('PC1'), ylabel('PC2')
    
   
    
    
    
    
end


end



