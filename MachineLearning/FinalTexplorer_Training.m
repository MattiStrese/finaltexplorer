function [ centroids,featureSpace ] = FinalTexplorer_Training( db )

t = cputime;
    % Ensure correct number of inputs
    if( nargin~=  1), help FinalTexplorer_Training; 
        return; 
    end; 

    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
       FinalTexplorer_Constants 
    end

    
    % local variables
    centroids    = zeros(numTextures,numFeatures);
    featureSpace = [];


    
    %% calculation section
    
    %% build plain file, rows are instances, columns are calculated features
    for k=1:(numQueries*numTextures)
        disp(strcat(num2str(k/(numQueries*numTextures)*100),' % calculated.'));
        [tactileFeatures] = FinalTexplorer_ProcessQuery(db{k}.data,db{k}.name,0);
        featureSpace = [featureSpace , tactileFeatures];

    end
    


    featureSpace = featureSpace';

    % calc mean for each numQueries following columns
    for m=1:numTextures

        beginS = m * numQueries - (numQueries-1);
        endS   = m * numQueries;


        tmpMatr = featureSpace(beginS:endS,:);

        if numQueries == 1
            centroids(m,:) = (tmpMatr);
        else
            centroids(m,:) = mean(tmpMatr);
        end
    end
    
    save(strcat(machinelearningPath,'FeatureSpace.mat'),'featureSpace' );
    save(strcat(machinelearningPath,'FeatureCentroids.mat'),'centroids');
    
%     % global normalisation 
% 
%     n = numFeatures;
%     minima = zeros(n,1);
%     maxima = zeros(n,1);
%     
%     for k=1:n
%        minima(k,1) = min(featureSpace(:,k));
%        maxima(k,1) = max(featureSpace(:,k));
%     end
% 
%     for l=1:n
%         for m=1:numInstances
%             featureSpace(m,l) = ML_Normalise(featureSpace(m,l),minima(l,1),maxima(l,1));
%         end
%     end
%     save(strcat(machinelearningPath,'Training_NormalisationMinima.mat'),'minima');
%     save(strcat(machinelearningPath,'Training_NormalisationMaxima.mat'),'maxima');
%     
%     save(strcat(machinelearningPath,'Training_Centroids.mat'),'centroids');
%     save(strcat(machinelearningPath,'Training_featureSpace.mat'),'featureSpace');

    disp('Used time:')
    e = cputime-t
end

