function [ CBTR_5Dim_Testing_TextureCentroids,CBTR_5Dim_Testing_PlainFile ] = CBTR_5Dim_Testing( acc,accTap,snd,sndTap,img,fric,rfl )
%   Description:        
%       This function calculates the Testing and test data sets for
%       further Machine Learning processing and evaluation
%
%   Input:  acceleration struct 
%   Output: 
%           ...
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................



    % Ensure correct number of inputs
    if( nargin~=  7), help CBTR_5Dim_Testing; 
        return; 
    end; 

    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
       CBTR_Constants 
    end

    
    % local variables
    CBTR_5Dim_Testing_TextureCentroids    = zeros(numTextures,numCombinedFeatures,2);
    CBTR_5Dim_Testing_PlainFile           = [];


    
    %% calculation section
    
    %% build plain file, rows are instances, columns are calculated features
    for k=1:(numQueries*numTextures)
        disp(strcat(num2str(k/(numQueries*numTextures)*100),' % calculated.'));
        tmp =  CBTR_5Dim_ProcessQuery( acc(k).dft321.dataTesting,...
            accTap(k).dataTesting,...
            snd(k).dataTesting,...
            sndTap(k).dataTesting,...
            img(k).noFlash.dataTesting,...
            img(k).flash.dataTesting,...
            fric(k).FSR1.dataTesting,...
            fric(k).FSR2.dataTesting,...
            rfl(k).dataTesting,'',0 );
        %tmp =  abs(0.00001 .* randn(9,1));
        CBTR_5Dim_Testing_PlainFile = [CBTR_5Dim_Testing_PlainFile , tmp];
        
        %% only add if more than 10 queries per textures a required.
        % add random noise to previous instance line and save it, just makes it
        % harder to classify - random noise is a good test for robustness
        % for the entire algorithm
        
        %%%%%%%CBTR_5Dim_Testing_PlainFile = [CBTR_5Dim_Testing_PlainFile , tmp];

    end
    CBTR_5Dim_Testing_PlainFile = CBTR_5Dim_Testing_PlainFile';



    % calc mean for each numQueries following columns
    for m=1:numTextures

        beginS = m * numQueries - (numQueries-1);
        endS   = m * numQueries;


        tmpMatr = CBTR_5Dim_Testing_PlainFile(beginS:endS,:);

     
        CBTR_5Dim_Testing_TextureCentroids(m,:,1) = mean(tmpMatr);
        %tmpVec = q(beginS:endS,11);
        %tmpVec(tmpVec == 0) = [];
        %CBTR_5Dim_Testing_TextureCentroids(m,11) = mean(tmpVec);
    end

    
    save 'MatlabData/CBTR_5Dim_Testing_PlainFileWithoutNormalisation.mat' CBTR_5Dim_Testing_PlainFile
    
    % global normalisation 

    n = numCombinedFeatures;
    minima = zeros(n,1);
    maxima = zeros(n,1);
    
    for k=1:n
       minima(k,1) = min(CBTR_5Dim_Testing_PlainFile(:,k));
       maxima(k,1) = max(CBTR_5Dim_Testing_PlainFile(:,k));
    end

    for l=1:n
        for m=1:numInstances
            CBTR_5Dim_Testing_PlainFile(m,l) = CBTR_Normalise(CBTR_5Dim_Testing_PlainFile(m,l),minima(l,1),maxima(l,1));
        end
    end
    save 'MatlabData/CBTR_5Dim_Testing_NormalisationMinima.mat' minima
    save 'MatlabData/CBTR_5Dim_Testing_NormalisationMaxima.mat' maxima
    
    
    save 'MatlabData/CBTR_5Dim_Testing_TextureCentroids.mat' CBTR_5Dim_Testing_TextureCentroids
    save 'MatlabData/CBTR_5Dim_Testing_PlainFile.mat' CBTR_5Dim_Testing_PlainFile


end

