function [ features ] = FinalTexplorer_ProcessQuery( materialRecording,ttitle,plotIt ) %
%   Description:
%       This function processes a datatrace
%
%   Input:  data trace
%           title
%           flag for plotting
%   Output:
%           ...
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................


% Ensure correct number of inputs
if( nargin~=  3), help FinalTexplorer_ProcessQuery;
    return;
end

% Check, if FinalTexplorer_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    FinalTexplorer_Constants
end
try
    
    materialRecording = materialRecording.materialRecording;
    accelX = materialRecording.accelX;
    accelY = materialRecording.accelY;
    accelZ = materialRecording.accelZ; 
    accelDFT = materialRecording.accelDFT;
    %normalForce = materialRecording.normalForce;
    FSR1Friction = materialRecording.FSR1Friction;
    FSR2Friction = materialRecording.FSR2Friction;
    reflectance = materialRecording.reflectance;
    soundRecording = materialRecording.soundRecording(:,1);
    metalDetection = materialRecording.metalDetection;
    temperatureData = materialRecording.temperatureData;
    stiffnessData = materialRecording.stiffness;
    stiffnessData2 = materialRecording.stiffness2;
    img = materialRecording.img{1};
catch exc

    accelX = materialRecording.accelX;
    accelY = materialRecording.accelY;
    accelZ = materialRecording.accelZ; 
    accelDFT = materialRecording.accelDFT;
    %normalForce = materialRecording.normalForce;
    FSR1Friction = materialRecording.FSR1Friction;
    FSR2Friction = materialRecording.FSR2Friction;
    reflectance = materialRecording.reflectance;
    soundRecording = materialRecording.soundRecording(:,1);
    metalDetection = materialRecording.metalDetection;
    temperatureData = materialRecording.temperatureData;
    stiffnessData = materialRecording.stiffness;
    stiffnessData2 = materialRecording.stiffness2;
    img = materialRecording.img{1};
end





tactileFeatures    = zeros(numTactileFeatures,1);
nonTactileFeatures = zeros(numNonTactileFeatures,1);

tic;
disp(' ')
disp('--------------------------------------------------------')
disp(ttitle);
disp('--------------------------------------------------------')






%% Tactile Features Calculation

% Macroscopic Roughness
disp('Macroscopic Roughness Features:');
%img = imread('c.png');
[tactileFeatures(1),tactileFeatures(2),tactileFeatures(3)] = FinalTexplorer_MacroscopicRoughness(accelDFT, reflectance,FSR1Friction,FSR2Friction, img, ttitle, plotIt);

for k=1:3
    disp(strcat('Feature:...',featureNames{k},'...',num2str(tactileFeatures(k,1))))
end


% Microscopic Roughness Features
disp('Microscopic Roughness Features:');
[tactileFeatures(4), tactileFeatures(5)] =   FinalTexplorer_MicroscopicRoughness( accelDFT, soundRecording, ttitle ,plotIt);                                   

for k=4:5
    disp(strcat('Feature:...',featureNames{k},'...',num2str(tactileFeatures(k,1))))
end

% Friction
disp('Friction Features:');
[tactileFeatures(6), tactileFeatures(7), tactileFeatures(8)]  = FinalTexplorer_Friction(  FSR1Friction,FSR2Friction,ttitle ,plotIt);                                   
for k=6:8
    disp(strcat('Feature:...',featureNames{k},'...',num2str(tactileFeatures(k,1))))
end

% Hardness Features
disp('Hardness Features:');
[tactileFeatures(9:13)]                         = FinalTexplorer_Hardness(accelX, soundRecording, stiffnessData, stiffnessData2 ,ttitle ,plotIt);                                   
%[0.9;0.2;0.5;0.1;0.1];
for k=9:13
    disp(strcat('Feature:...',featureNames{k},'...',num2str(tactileFeatures(k,1))))
end



% Warmth
disp('Warmth Features:');
[tactileFeatures(14), tactileFeatures(15)]                      = FinalTexplorer_Warmth(temperatureData , ttitle ,plotIt);                                   
for k=14:15
    disp(strcat('Feature:...',featureNames{k},'...',num2str(tactileFeatures(k,1))))
end


%% Tactile Features Calculation



nonTactileFeatures(1) = FinalTexplorer_IsMetalFeature(metalDetection,ttitle,plotIt);
nonTactileFeatures(2) = 0;


disp('')
disp('Nontactile Features:');
for k=1:numNonTactileFeatures
    disp(strcat('Feature:...',featureNames{numTactileFeatures+k},'...',num2str(nonTactileFeatures(k,1))))
end


%range limits
for k=1:numTactileFeatures
    if tactileFeatures(k,1) > 1
        tactileFeatures(k,1) = 1;
    end
    if tactileFeatures(k,1) < 0
        tactileFeatures(k,1) = 0;
    end    
end

for k=1:numNonTactileFeatures
    if nonTactileFeatures(k,1) > 1
        nonTactileFeatures(k,1) = 1;
    end
    if nonTactileFeatures(k,1) < 0
        nonTactileFeatures(k,1) = 0;
    end    
end 




features           = [tactileFeatures;nonTactileFeatures];
timeUsed = toc;
disp(strcat('Calculation Time...',num2str(timeUsed),'sec'));



end

