function [ f,X ] = CBTR_PrettyPlotter( x, ttitle, fs, X_Max,varargin)
%CBTR_PLOTTER Summary of this function goes here
%   Detailed explanation goes here

% try 
%     x = x((3*fs)+1 : 4*fs);
% catch exc
%     
% end



%% row column correction
[m,n] = size(x);
if m > n
    x = x';
end


%% FFT
Ts = 1/fs;
N = length(x);
f = [0:floor( (N-1)/2)] / (N*Ts);
X = fft(x);
X = X / N;
X = [X(1) 2*X(2:floor((N-1)/2) + 1) ];



%% 1-D DCT
% https://de.mathworks.com/help/signal/ref/dct.html
X_dct = abs(dct(x,round(fs/2))) * sqrt(2)./sqrt(N);


%% filtered signal
fstop1 = 5;
fstop2 = 90;
fHigh  = 100;
%[b,a] = butter(3,[fstop1/(fs/2) fstop2/(fs/2)],'stop');
[b,a] = butter(5,fHigh / (fs/2),'high');
xfilt = filter(b,a,x);
Xfilt = fft(xfilt);
Xfilt = Xfilt / N;
Xfilt = [Xfilt(1) 2*Xfilt(2:floor((N-1)/2) + 1) ];






%% Plots
figure('units','normalized','outerposition',[0 0 1 1]);
subplot(211)
plot(x,'black');
title(strcat(ttitle,'...Time Domain'));
if nargin == 4
    axis([-inf inf -0.5 0.5])
else
    axis([-inf inf -varargin{1} varargin{1}])
end
xlabel('Sample Point')
ylabel('Acc (g)')
subplot(212)
stem(f,abs(X),'.black')
title(strcat(ttitle,'...Spectral Domain'))
axis([0 round(fs/4) -inf X_Max])
xlabel('f (Hz)')
ylabel('X(f)')


% figure;
% subplot(211)
% plot(x,'black');
% title('Time Domain')
% subplot(212)
% stem(X_dct,'.black');
% axis([0 1200 -inf X_Max]);
% title('DCT Spectral Domain')


% Prints
set(gcf,'PaperUnits','centimeters');
set(gcf,'PaperSize',[40 10.2]);                  %[12 4.2]);
set(gcf,'PaperPosition',[-4.0,0.00,48,10.2]);  %[-0.5,0.20,12.7,4.0]);
set(gcf,'PaperPositionMode','manual');
tmp = strsplit(ttitle,'.');
print(gcf,'-dpdf',strcat('Plots\\WHC2017\\Acc\\Signal...',tmp{1},'.pdf'));
%print(gcf,'-dpdf',strcat('Signal...',tmp{1},'.pdf'));



% figure;
% subplot(211)
% plot(xfilt,'black');
% title(strcat(ttitle,'...(filtered) Time Domain'));
% axis([-inf inf -0.5 0.5])
% xlabel('Sample Point')
% ylabel('Acc (g)')
% subplot(212)
% stem(f,abs(Xfilt),'.black')
% title(strcat(ttitle,'...(filtered) Spectral Domain'));
% axis([0 round(fs/4) -inf X_Max])
% xlabel('f (Hz)')
% ylabel('X(f)')
% 
% % Prints filtered
% set(gcf,'PaperUnits','centimeters');
% set(gcf,'PaperSize',[40 10.2]);                  %[12 4.2]);
% set(gcf,'PaperPosition',[-4.0,0.00,48,10.2]);  %[-0.5,0.20,12.7,4.0]);
% set(gcf,'PaperPositionMode','manual');
% tmp = strsplit(ttitle,'.');
% %print(gcf,'-dpdf',strcat('Plots\\WHC2017\\Acc\\Signal...',tmp{1},'filtered.pdf'));
% %print(gcf,'-dpdf',strcat('FilteredSignal...',tmp{1},'.pdf'));


end

