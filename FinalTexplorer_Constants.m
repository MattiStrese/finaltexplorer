%https://de.mathworks.com/help/matlab/matlab_env/add-remove-or-reorder-folders-on-the-search-path.html


PORT = 'COM74';
devName = 'Dev6';


%% paths
dbPath                              = strcat(pwd,'/Database/');

accPath                             = strcat(dbPath,'acceleration/');
accTapPath                          = strcat(dbPath,'accelerationTap/');

fricPath                            = strcat(dbPath,'friction/');
metalPath                           = strcat(dbPath,'metaldetection/');
reflectancePath                     = strcat(dbPath,'reflectance/');
soundPath                           = strcat(dbPath,'sound/');
soundTapPath                        = strcat(dbPath,'soundTap/');

stiffnessPath                       = strcat(dbPath,'stiffness/');
thermalPath                         = strcat(dbPath,'thermal/');
imagePath                           = strcat(dbPath,'image/');

materialMatPath                     = strcat(dbPath,'materialMat/');


tactileModelsPath                   = strcat(dbPath,'TactileFeatures/Models/');


visPath                             = strcat(pwd,'/Visualization/pdf/');

machinelearningPath                 = strcat(pwd,'/MachineLearning/');



rawMatPath                          = strcat(dbPath,'raw/mat/');
rawPngPath                          = strcat(dbPath,'raw/png/');
rawWavPath                          = strcat(dbPath,'raw/wav/');

mdlImgPath                          = strcat(dbPath,'MaterialModel/Images/');
mdlParamPath                        = strcat(dbPath,'MaterialModel/Parameters/');
mdlParamJsonPath                    = strcat(mdlParamPath,'json/');
mdlParamTxtPath                     = strcat(mdlParamPath,'txt/');
mdlParamMatPath                     = strcat(mdlParamPath,'mat/');

mdlTacSignPath                      = strcat(dbPath,'MaterialModel/TactileSignals/');
mdlTacSignMatPath                   = strcat(mdlTacSignPath,'mat/');
mdlTacSignTxtPath                   = strcat(mdlTacSignPath,'txt/');
mdlTacSignWavPath                   = strcat(mdlTacSignPath,'wav/');


%% Scan settings


FS_ACCEL = 3000;
FS_SOUND = 44100;
FS_OTHER_SENSORS = 1250;






%% General Settings
numTextures                         = 2;%108;
numQueries                          = 1;
numInstances                        = numQueries * numTextures;



numMacroFeatures                    = 3;
numMicroFeatures                    = 2;
numComplianceFeatures               = 5;
numFrictionFeatures                 = 3;
numThermalFeatures                  = 2;

numTactileFeatures                  = numMacroFeatures + numMicroFeatures + numComplianceFeatures + numFrictionFeatures + numThermalFeatures;
numNonTactileFeatures               = 2;
numFeatures                         = numTactileFeatures + numNonTactileFeatures;

featureNames     = {};
featureNames{1}  = 'Macrotexture (mTX)            :';
featureNames{2}  = 'Macrotexture Coarseness (mCO) :';
featureNames{3}  = 'Macrotexture Regularity (mRG) :';
featureNames{4}  = 'Microtexture Roughness (uRO)  :';
featureNames{5}  = 'Microtexture Coarseness (uCO) :';
featureNames{6}  = 'Tactile Stiction (fST)        :';
featureNames{7}  = 'Sliding Resistance (fRS)      :';
featureNames{8}  = 'Adhesive Tack (aTK)           :';
featureNames{9}  = 'Tactile Compliance (cCM)      :';
featureNames{10} = 'Local Deformation (cDF)       :';
featureNames{11} = 'Damping (cDP)                 :';
featureNames{12} = 'Relaxation (cRX)              :';
featureNames{13} = 'Yielding (cYD)                :';
featureNames{14} = 'Thermal Cooling (tCO)         :';
featureNames{15} = 'Thermal Persistence (tPR)     :';

featureNames{16} = 'is metallic     (iMe)         :';
featureNames{17} = 'Color (col)                   :';





%% accel specific constants
CBAR_TAP_OFFSET                     = 1500;

CBAR_SAMPLERATE                     = 10000;
CBAR_ENERGYDETECTIONTHRESHOLD       = 5*10^-2;
CBAR_NOMOVEMENTTHRESHOLD            = 5*10^-3;
CBAR_REMOVALDISTANCE                = 5000;
CBAR_REMOVALFRAME1                  = 500;  % 1000
CBAR_REMOVALFRAME2                  = 500;  % 1000

%% sound specific constants
% energy detection threshold
CBSR_SAMPLERATE                     = 44100;
CBSR_ENERGYDETECTIONTHRESHOLD       = 5*10^-1;
CBSR_NOMOVEMENTTHRESHOLD            = 1*10^-3;
CBSR_REMOVALDISTANCE                = 5000;
CBSR_REMOVALFRAME1                  = 500;  % 1000
CBSR_REMOVALFRAME2                  = 500;  % 1000


%% CBTR_MAIN global variables

accuracyCounter = 0;
sumConfMat       = zeros(numTextures,numTextures);


%% selectedFeatures


%only 5dim features
selectedFeatures = [1:15];


%% Used data sets, generated (training and testing data)

tmp = load('labelNames108.mat');
y = unique(tmp.labelNames108);



y108 = load('labelNames108.mat');
Y108  = y108.labelNames108;


% mapping: 1...numTextures --> texture name
Y2 = Y108(1:numQueries:numInstances);
YNum = 1:numTextures;
YNum = repmat(YNum,10,1);
YNum = YNum(:);

predictedIndices = zeros(numTextures,1);
predictedSimilarClassesFromClassification ={};

%% Retrieval
numSimilarTextures = 3;
retrievalPath  = strcat(pwd,'Retrieval\\');
similarSurfaces = load('similarityStruct.mat');


%tmp = load('MatlabData/dataCentroids.mat');
%dataCentroids = tmp.dataCentroids;



% todo
try
    x = load('featureSpace_Training.mat'); %queryFeatureVectors.mat
    featureSpace_Training = abs(x.featureSpace_Training);
    noiseVec = 0.000001*randn(size(featureSpace_Training));
    %%.. add minimal noise = harder to classify!!!
    %featureSpace_Training = featureSpace_Training + noiseVec;
    %featureSpace_Training = abs(featureSpace_Training);


    x = load('featureSpace_Testing.mat'); %queryFeatureVectors.mat
    featureSpace_Testing = abs(x.featureSpace_Testing);
    noiseVec = 0.000001*randn(size(featureSpace_Testing));
    %%.. add minimal noise = harder to classify!!!
    featureSpace_Testing = featureSpace_Testing + noiseVec;
    featureSpace_Testing = abs(featureSpace_Testing);
catch exc
    
end



