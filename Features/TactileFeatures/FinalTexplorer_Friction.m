function [ tactileStiction, slidingResistance, adhesiveTack ] = FinalTexplorer_Friction( fsr1,fsr2,ttitle , plotIt)
% Friction
% 
% Tactile Stiction (fST) � The effort required to initiate sliding on a surface, ranging from low grip to high grip.
% 
% Sliding Resistance (fRS) � The effort required to continue sliding over a surface, ranging from slippery to resistive.
%
% 
% Adhesive
% 
% Adhesive Tack (aTK) � The effort required to break contact with a surface, ranging from no adhesion to sticky.
% 
%   Author: Matti Strese 2017

%% PRELIMINARIES %%


% Ensure correct number of inputs
if( nargin~=  4), help FinalTexplorer_Friction;
    return;
end;
% Check, if FinalTexplorer_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    FinalTexplorer_Constants
end
%% output
tactileStiction  = 0;
slidingResistance  = 0;
adhesiveTack  = 0;

%figure; plot(normalForce)
%figure; plot(fsr2); title('Normal Force')
%figure; plot(fsr1); title('Friction Force')


%% Feature: tactileStiction
data = abs(fsr1./ fsr2);  % 0.001 for kg
%todo find good segmentation
data = data(1000:end-1000);



tactileStiction = max(abs(data));%/(1+0.001*mean(normalForce));

if plotIt == 6
    figure; plot(data); % hold on; plot(normalForce);
    
    title(strcat(featureNames{plotIt},num2str(tactileStiction)))
    axis([-inf inf 0 2])
end

slidingResistance = 0.9*tactileStiction;%- 0.1*estimatedVelocity;%  - 0.1*mean(abs(fricLeft)+abs(fricRight)) ;

if plotIt == 7
    figure; plot(data); 
    title(strcat(featureNames{plotIt},num2str(slidingResistance),'...',ttitle))
    axis([-inf inf 0 5])
end


adhesiveTack = tactileStiction;%- 0.1*estimatedVelocity;%  - 0.1*mean(abs(fricLeft)+abs(fricRight)) ;

if plotIt == 8
    figure; plot(data); 
    title(strcat(featureNames{plotIt},num2str(adhesiveTack),'...',ttitle))
    axis([-inf inf 0 5])
end

end




















