function [ microtextureRoughness, microtextureCoarseness ] = FinalTexplorer_MicroscopicRoughness( acc, snd, ttitle , plotIt, varargin)
% 
% Microtexture
% 
% Microtexture Roughness (uRO) � The intensity of small features (<1mm spacing) that creates the 
% perception of roughness ranging from smooth to rough
% 
% Microtexture Coarseness (uCO) � The perceived spacing of small features (<1mm spacing), ranging from fine to coarse.
%   Author: Matti Strese 2017

%% PRELIMINARIES %%

% Ensure correct number of inputs
%if( nargin~=  4), help CBTR_5Dim_MicroscopicRoughness;
%    return;
%end;
% Check, if CBTR_Constants has already been loaded for global variablclces
if ( any(strcmp(who,'numTextures')) == 0)
    FinalTexplorer_Constants
end




%% output
microtextureRoughness = 0;
microtextureCoarseness = 0;


if nargin == 4

    %% parameters
    % MiR Acc
    MiAR_highFilter = 20;
    MiAR_lowFilter = 1200;
    MiAR_scaler = 100;
    MiAR_VarScaler = 0.00001;

    % MiR Snd
    MiSR_highFilter = 100;
    MiSR_lowFilter = 12000;
    MiSR_scaler = 100;
    MiSR_VarScaler = 0.00001;

else
    microRoughParameters = varargin{1};
    % MiR Acc
    MiAR_highFilter = microRoughParameters.MiAR_highFilter;
    MiAR_lowFilter = microRoughParameters.MiAR_lowFilter;
    MiAR_scaler = microRoughParameters.MiAR_scaler;
    MiAR_VarScaler = microRoughParameters.MiAR_VarScaler;

    % MiR Snd
    MiSR_highFilter = microRoughParameters.MiSR_highFilter;
    MiSR_lowFilter = microRoughParameters.MiSR_lowFilter;
    MiSR_scaler = microRoughParameters.MiSR_scaler;
    MiSR_VarScaler = microRoughParameters.MiSR_VarScaler;
end






%wtype = 'coif3';
wtype = 'db6';





[b1, a1] = butter(7,MiAR_highFilter/(CBAR_SAMPLERATE/2),'high');
[b2, a2] = butter(5,MiSR_highFilter/(CBSR_SAMPLERATE/2),'high');
acc = filter(b1,a1,acc);
snd = filter(b2,a2,snd);

[b1, a1] = butter(7,MiAR_lowFilter/(CBAR_SAMPLERATE/2),'low');
[b2, a2] = butter(5,MiSR_lowFilter/(CBSR_SAMPLERATE/2),'low');
acc = filter(b1,a1,acc);
snd = filter(b2,a2,snd);






%% Feature Micro Accel Roughness microtextureRoughness 
[C,L] = wavedec(acc,5,wtype);

D1Acc = wrcoef('d',C,L,wtype, 1);
D2Acc = wrcoef('d',C,L,wtype, 2);
D3Acc = wrcoef('d',C,L,wtype, 3);
D4Acc = wrcoef('d',C,L,wtype, 4);
D5Acc = wrcoef('d',C,L,wtype, 5);
%A5Acc = wrcoef('a',C,L,wtype, 5);
%figure; plot(0.8+A5Acc,'red'); hold on
%plot(acc);


yAcc = abs(D1Acc);
zAcc = abs(D5Acc);
scaler = (mean(zAcc))/(mean(yAcc));
yAcc = yAcc.*scaler;
difference = abs(yAcc-zAcc);

microtextureRoughness = log10(1 + MiAR_scaler * mean(difference)) - MiAR_VarScaler * var(acc);




%% Feature Micro Sound Roughness microtextureCoarseness 

[C,L] = wavedec(snd,5,wtype);

D1Snd = wrcoef('d',C,L,wtype, 1);
D2Snd = wrcoef('d',C,L,wtype, 2);
D3Snd = wrcoef('d',C,L,wtype, 3);
D4Snd = wrcoef('d',C,L,wtype, 4);
D5Snd = wrcoef('d',C,L,wtype, 5);



ySnd = abs(D1Snd);
zSnd = abs(D5Snd);
scaler = (mean(zSnd))/(mean(ySnd));
ySnd = ySnd.*scaler;
difference = abs(ySnd-zSnd);
microtextureCoarseness = log10(1 + MiSR_scaler * mean(difference)) - MiSR_VarScaler * var(snd);







if plotIt == 4 || plotIt == 5
    figure;
    subplot(211)
    plot(abs(yAcc),'r');
    hold on;
    plot(abs(zAcc),'black');
    title(strcat(featureNames{plotIt},num2str(microtextureRoughness),'...',ttitle))
    axis([-inf inf 0 1]);
    xlabel('Time (s)','FontSize',10,'FontName','Arial');
    ylabel('Amplitude (g)','FontSize',10,'FontName','Arial');
    h = legend('$$D_1$$','$$Scaled \: D_5$$','Location','northeast');
    set(h,'Interpreter','latex','fontsize',9)
    subplot(212)
    plot(abs(ySnd),'r');
    title(strcat(featureNames{plotIt},num2str(microtextureCoarseness),'...',ttitle))
    hold on;
    plot(abs(zSnd),'black');
    axis([-inf inf 0 1]);
%     if printIt == 1
%         paperX = 12.0;                  %# A3 paper size
%         paperY = 4;                  %# A3 paper size
%         xMargin = 0.01;               %# left/right margins from page borders
%         yMargin = 0.01;               %# bottom/top margins from page borders
%         xSize = paperX - 2*xMargin;     %# figure size on paper (width & height)
%         ySize = paperY - 2*yMargin;     %# figure size on paper (width & height)
%         
%         set(gcf,'PaperUnits','centimeters');
%         set(gcf,'PaperSize',[paperX paperY]);
%         set(gcf,'PaperPosition',[xMargin yMargin xSize ySize]);
%         set(gcf,'PaperPositionMode','manual');
%         print(gcf,'-dpdf',strcat('CBAR_Plots\\','MicroRoughness...',ttitle,'.pdf'));
%     end
end





end

