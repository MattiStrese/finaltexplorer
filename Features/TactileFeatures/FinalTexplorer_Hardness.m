function [ cCM, cDF, cDP, cRX, cYD ] = FinalTexplorer_Hardness( accelX, soundRecording, stiffnessData1, stiffnessData2 ,ttitle ,plotIt)   
% 
% Compliance
% 
% Tactile Compliance (cCM) � The degree that a surface deforms under pressure, from rigid to compliant.
% 
% Local Deformation (cDF) � The degree to which the surface wraps around the fingertip when being deformed, 
% ranging stays flat to high wrap.
% 
% Damping (cDP) � The speed that a surface returns to its original shape after being deformed, ranging from 
% springy to damped.
% 
% Relaxation (cRX) � The degree to which a surface stops pushing back after being deformed, ranging from 
% maintains force to relaxes.
% 
% Yielding (cYD) � The degree to which a surface remains deformed after being pressed, ranging from recovers 
% shape to stays deformed.
%   Author: Matti Strese 2014

%% PRELIMINARIES %%
% Check, if FinalTexplorer_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    FinalTexplorer_Constants
end

%% output
cCM = 0;
cDF = 0;
cDP = 0;
cRX = 0;
cYD = 0;



stiffnessData1 = stiffnessData1 *(9.81/1000);
stiffnessData2 = stiffnessData2 *(9.81/1000);
figure; plot(stiffnessData1); hold on; plot(stiffnessData2,'black');






averageStiffness = 0.5*(stiffnessData1 + stiffnessData2);

minVal = min(averageStiffness)
averageStiffness(averageStiffness < 1.1*minVal) = [];
[maxVal,maxIdx] = max(averageStiffness)


% todo scaler
scaler = 0.2;

if maxIdx > 1
    cCM = scaler * (maxVal - minVal)  /  (maxIdx-1)
else

    cCM = 0;
end
    
if plotIt == 9
    figure;  plot(averageStiffness,'red') 
    title(strcat(featureNames{plotIt},num2str(cCM),'...',ttitle))
    axis([-inf inf 0 15]);
    xlabel('Sample');
    ylabel('Force (N)')
end








end


