function [ X,f, max3Freq, max3Amplitudes, noisePower ] = Acc_SignalSpectrum( x, ttitle, useWindowing, plotIt, amplX )
% t = 0:1/10000:5;
% x = cos(2*500*pi*t);
% CBSR_SignalSpectrum(x,10000,'',2);
%https://de.mathworks.com/matlabcentral/newsreader/view_thread/166258
max3Freq = []; 
max3Amplitudes = [];
noisePower = 0;
doNotUseNextPow2 = 1;

originalx = x;
fs = 10000;
T = 1/fs;
N = length(x);
t = (0:N-1)*T;

if useWindowing == 1    
    w = hanning(length(t));
    x = w.*x; 
end
if doNotUseNextPow2 == 1
    NFFT = N; %2^nextpow2(N)
else
    NFFT = 2^nextpow2(N);  
end

X = fft(x,NFFT);

% for visualization: half of spectrum: X_h
NumUniquePts = ceil((NFFT+1)/2);
X_h = X(1:NumUniquePts);
X_h = 2*X_h/N;
absX = abs(X_h);

f = (0:NumUniquePts-1)*fs/NFFT;

frequencyResolution = fs / NFFT


% get dominant frequencies

    maxAbsX = max(absX);
    [maxVal, maxInd] = sort(absX,'descend');
    absXLower = absX;
    absXUpper = absX;
    absXLower(absX > 0.5*maxAbsX) = 0;
    absXUpper(absX <= 0.5*maxAbsX) = 0;
    max3Freq = maxInd(1:10)';
    max3Amplitudes = maxVal(1:10)';
    
    [~,maxIndF] = sort(max3Freq,'ascend');
    max3Freq = max3Freq(maxIndF);
    max3Amplitudes = max3Amplitudes(maxIndF);
    tmp = [0 max3Freq];
    
    % JND correction, remove too close frequencies
    corrected_JND_indices = find(diff(tmp)>5);
    
    max3Freq = max3Freq(corrected_JND_indices);
    max3Amplitudes = max3Amplitudes(corrected_JND_indices);
    
    noisePower = mean(absXLower(1:600));%/max(absXUpper)

if plotIt == 1
    figure;
    subplot(211)
    plot(originalx,'black'); title('Time domain');
    
    subplot(212)
    stem(f,absX,'.black'); title('Spectral Domain');
    axis([0 600 0 amplX])
    % Prints
    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperSize',[40 10.2]);                  %[12 4.2]);
    set(gcf,'PaperPosition',[-4.0,0.00,46,10.2]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    %tmp = strsplit(ttitle,'.');
    %print(gcf,'-dpdf',strcat('pdfplots/SignalSpectrum...',ttitle,'.pdf'));    
elseif plotIt == 2

    figure;
    subplot(211);

    stem(f,absXUpper,'.black'); title('Spectral Domain (Upper)');
    axis([0 650 0 amplX])
    xlabel('Frequency (Hz)')
    ylabel('X(f)')    

    subplot(212)

    stem(f,absXLower,'.black'); title('Spectral Domain (Lower)');
    axis([0 650 0 amplX]);
    xlabel('Frequency (Hz)')
    ylabel('X(f)')
    
      
    save(strcat('surfaceMaterialModel/',ttitle,'_freq.mat'), 'max3Freq')
    save(strcat('surfaceMaterialModel/',ttitle,'_ampl.mat'), 'max3Amplitudes')
    save(strcat('surfaceMaterialModel/',ttitle,'_noise.mat'), 'noisePower')
    
    % Prints
    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperSize',[40 10.2]);                  %[12 4.2]);
    set(gcf,'PaperPosition',[-4.0,0.00,46,10.2]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    %tmp = strsplit(ttitle,'.');
    %print(gcf,'-dpdf',strcat('pdfplots/SignalSpectrum...',ttitle,'.pdf'));
end

