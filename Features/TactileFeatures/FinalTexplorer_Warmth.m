function [ thermalCooling, thermalPersistence ] = FinalTexplorer_Warmth( tempData, ttitle , plotIt)
%   Description
%
%   %Thermal
%
%   Thermal Cooling (tCO) � The initial rate that a surface draws heat from the fingertip, ranging from warm to cool.
%
%   Thermal Persistence (tPR) � The extent that a surface continues to draw heat from the fingertip, 
%   ranging from transient cooling to sustained cooling.
%
%   Author: Matti Strese 2017

%% PRELIMINARIES %%


% Ensure correct number of inputs
if( nargin~=  3), help FinalTexplorer_Warmth;
    return;
end;
% Check, if FinalTexplorer_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    FinalTexplorer_Constants
end

%figure;plot(tempData)


%% output
thermalCooling = 0;
thermalPersistence = 0;



[ambientTemperature,~] = min(tempData);

[maxVal,maxInd] = max(tempData);

if maxInd < numel(tempData)
    tempData = tempData(maxInd:end);
end

UPPER_TEMPERATURE_LIMIT = 50;

thermalCooling = 1 - (  (maxVal - ambientTemperature) / (UPPER_TEMPERATURE_LIMIT - ambientTemperature)  );
if thermalCooling < 0
    thermalCooling = 0;
end
if thermalCooling > 1
    thermalCooling = 1;
end

t = 1:1:numel(tempData);
t = 0.01*t';
if plotIt == 14 
   figure;
   plot(t,tempData,'black'); title(strcat(featureNames{plotIt},num2str(thermalCooling)));
   %hold on
   %plot(f,'r'); title(strcat(ttitle,'...',num2str(thermalCooling)));
   axis([-inf inf 22 35])
   
    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperSize',[40 10.2]);                  %[12 4.2]);
    set(gcf,'PaperPosition',[-4.0,0.00,48,10.2]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    tmp = strsplit(ttitle,'.');
    %print(gcf,'-dpdf',strcat('Visualization\\WaG_',tmp{1},'.pdf'));
end




% x = (0:0.2:5)';
% y = 2*exp(-0.2*x) + 0.1*randn(size(x));
% f = fit(x,y,'exp1')
% figure;
% plot(f,x,y)

t = 1:1:numel(tempData);
t = 0.01*t';
tempData = tempData - tempData(end);
intpTempData = fit(t,tempData','exp1');

thermalPersistence = 1.0 - abs(intpTempData.b);
if thermalPersistence < 0
    thermalPersistence = 0;
end
if thermalPersistence > 1
    thermalPersistence = 1;
end


if plotIt == 15 
   figure;
   plot(intpTempData,t,tempData,'red'); hold on
   plot(t,tempData,'black');
   
   title(strcat(featureNames{plotIt},num2str(thermalPersistence)));
   %hold on
   %plot(f,'r'); title(strcat(ttitle,'...',num2str(thermalCooling)));
   axis([-inf inf 0 15])
   
    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperSize',[40 10.2]);                  %[12 4.2]);
    set(gcf,'PaperPosition',[-4.0,0.00,48,10.2]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    tmp = strsplit(ttitle,'.');
    %print(gcf,'-dpdf',strcat('Visualization\\WaG_',tmp{1},'.pdf'));
end


end

