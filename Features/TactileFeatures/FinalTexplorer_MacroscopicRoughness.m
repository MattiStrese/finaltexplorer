function [ macrotextureStrength, macrotextureCoarseness, macrotextureRegularity  ] = FinalTexplorer_MacroscopicRoughness( acc, rfl, fsr1,fsr2, img, ttitle,plotIt, varargin )
% 
% Macrotexture
% 
% Macrotexture (mTX) � The intensity of large features (>1mm spacing) that creates the perception of texture ranging from smooth to textured.
% 
% Macrotexture Coarseness (mCO) � The perceived spacing of large features (>1mm spacing), ranging from fine to coarse.
% 
% Macrotexture Regularity (mRG) � The perceived uniformity of large features, ranging from random to regular.
% 
%   Author: Matti Strese 2014

%% PRELIMINARIES %%


% Ensure correct number of inputs
%if( nargin~= 7 ), help CBTR_5Dim_MacroscopicRoughness;
%    return;
%end;
% Check, if CBTR_Constants has already been loaded for global variablclces
if ( any(strcmp(who,'numTextures')) == 0)
    FinalTexplorer_Constants
end


%% output
macrotextureStrength  = 0;
macrotextureCoarseness = 0;
macrotextureRegularity  = 0;




if nargin == 7

    %% parameters

    % Spikiness
    spikiness_filterThreshold = 100;
    spikiness_shift           = 1000;
    spikiness_varStrength     = 1;
    spikiness_meanStrength    = 1;
    spikiness_maxStrength     = 0.5;
    spikiness_scaler          = 50;

    % Friction Variance
    frictionVariance_shift               = 200;
    frictionVariance_multiplicatorFric   = 100;

    % Reflectance Variance
    reflectVariance_shift               = 1000;
    reflectVariance_multiplicatorRfl    = 50;

else
    macroRoughParameters = varargin{1};
    % Spikiness
    spikiness_filterThreshold = macroRoughParameters.spikiness_filterThreshold;
    spikiness_shift           = macroRoughParameters.spikiness_shift;
    spikiness_varStrength     = macroRoughParameters.spikiness_varStrength;
    spikiness_meanStrength    = macroRoughParameters.spikiness_meanStrength;
    spikiness_maxStrength     = macroRoughParameters.spikiness_maxStrength;
    spikiness_scaler          = macroRoughParameters.spikiness_scaler;

    % Friction Variance
    frictionVariance_shift               = macroRoughParameters.frictionVariance_shift;
    frictionVariance_multiplicatorFric   = macroRoughParameters.frictionVariance_multiplicatorFric;

    % Reflectance Variance
    reflectVariance_shift               = macroRoughParameters.reflectVariance_shift;
    reflectVariance_multiplicatorRfl    = macroRoughParameters.reflectVariance_multiplicatorRfl;
end












%% Feature) Macroscopic Roughness Strength macrotextureStrength

% a) Spikiness
[bLow, aLow]   = butter(7,spikiness_filterThreshold/(CBAR_SAMPLERATE/2),'low');
x = abs(filter(bLow,aLow,acc));
x = x./range(x);

b = ones(spikiness_shift,1)./spikiness_shift;
xAveraged = spikiness_varStrength*std(x) + spikiness_meanStrength * mean(x) + filter(b,1,abs(x));
xDiff = x( (1+spikiness_shift) : (end-spikiness_shift) ) - xAveraged( (1+2*spikiness_shift) : end);
xDiff(xDiff < 0) = 0;
spikiness = (abs(sum(abs(xDiff))) + spikiness_maxStrength*max(xDiff)) / spikiness_scaler;

% b) Friction Variance
frc            = abs(fsr1);%abs(fsr1 - fsr2);
movAvgOfDataFrc = tsmovavg(frc,'s',frictionVariance_shift,1);
frc            = frc(frictionVariance_shift+1:end-round(frictionVariance_shift/2)-frictionVariance_shift);
movAvgOfDataFrc = movAvgOfDataFrc(frictionVariance_shift+1+round(frictionVariance_shift/2):end-frictionVariance_shift);
diffFricData    = abs(frc - movAvgOfDataFrc);
slipstickValue  = log10(1+frictionVariance_multiplicatorFric * mean(diffFricData));


% c) Reflect Variance
movAvgOfData        = tsmovavg(rfl,'s',reflectVariance_shift,1);
rfl                 = rfl(reflectVariance_shift+1:end-round(reflectVariance_shift/2)-reflectVariance_shift);
movAvgOfData        = movAvgOfData(reflectVariance_shift+1+round(reflectVariance_shift/2):end-reflectVariance_shift);
diffReflectData     = abs(rfl - movAvgOfData);
reflectVar          = reflectVariance_multiplicatorRfl*mean(diffReflectData);





%macrotextureStrength = 0.01*(spikiness + slipstickValue + reflectVar);
% todo  check calculation
%spikiness
%reflectVar
macrotextureStrength = 0.2*spikiness * reflectVar;

if plotIt == 1
    figure;
    subplot(311)
   
    plot(x,'black');
    hold on;
    plot(spikiness_shift:numel(xDiff)-1,  xDiff(1:end-spikiness_shift)+xAveraged(spikiness_shift:numel(xDiff)-1),'green')
    plot(xAveraged,'-r');
    axis([-inf inf 0 4]);
    title(strcat('Spikiness:',num2str(spikiness),'...Slip:',num2str(slipstickValue),'...ReflVar:',num2str(reflectVar)))

    %title(strcat('Spikiness of...',ttitle,'...',num2str(spikiness)));
    xlabel('Sample','FontSize',12,'FontName','Arial');
    ylabel('Amplitude (g)','FontSize',12,'FontName','Arial');
    legend({'Acceleration Signal','Detected Spikes','Threshold'},'Location','Northeast')   
  
    
    subplot(312)
    plot(frc,'black'); axis([-inf inf 0 inf])
    hold on
    plot(movAvgOfDataFrc,'r');
    %title(strcat(ttitle,'...',num2str(reflectVar)));
    xlabel('Sample','FontSize',12,'FontName','Arial');
    ylabel('Friction (V)','FontSize',12,'FontName','Arial');
    legend({'Friction Signal','SMA_{1000}'},'Location','Northeast')
    hold off    
    
    subplot(313)
    plot(rfl,'black'); axis([-inf inf 0 5])
    hold on
    plot(movAvgOfData,'r');
    %title(strcat(ttitle,'...',num2str(reflectVar)));
    xlabel('Sample','FontSize',12,'FontName','Arial');
    ylabel('Reflectance (V)','FontSize',12,'FontName','Arial');
    legend({'Reflectance Signal','SMA_{1000}'},'Location','Northeast')
    hold off
    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperSize',[20 5.2]);                  %[12 4.2]);
    set(gcf,'PaperPosition',[-2.0,0.00,24,5.2]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    tmp = strsplit(ttitle,'.');
    %print(gcf,'-dpdf',strcat('Visualization\\macrotextureStrength_',tmp{1},'.pdf'));    

end














%% Feature) Macroscopic Roughness Regularity macrotextureRegularity

img = rgb2gray(img);
hcdPoints = detectSURFFeatures(img);

coarsenessStruct = selectStrongest(hcdPoints, 5);

% todo magic number 0.05
macrotextureCoarseness = 0.05 * mean(coarsenessStruct.Scale);
%coarsenessStruct.Metric

if plotIt == 2
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(121)
    imshow(img); title(strcat(featureNames{plotIt},':',num2str(macrotextureCoarseness)))
    subplot(122)
    imshow(img);
    title('Strongest Feature Points from Surface Image');
    hold on;
    plot(selectStrongest(hcdPoints, 5));
    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperSize',[40 10.2]);                  %[12 4.2]);
    set(gcf,'PaperPosition',[-4.0,0.00,48,10.2]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    tmp = strsplit(ttitle,'.');
    print(gcf,'-dpdf',strcat('Visualization\\macrotextureCoarseness_',tmp{1},'.pdf'));    

end




hcdPoints = detectSURFFeatures(img);
regularityStruct = selectStrongest(hcdPoints, 100);

xMaxima = diff(sort(regularityStruct.Location(:,1),'ascend'));
yMaxima = diff(sort(regularityStruct.Location(:,2),'ascend'));

% figure; plot(diff(xMaxima)); axis([-inf inf 0 50])

D = pdist2(xMaxima,yMaxima,'euclidean'); %calculate the distance between them

macrotextureRegularity = 0.9 * (std(D)/(1+mean(D))) ;


if plotIt == 3
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(121)
    imshow(img); title(strcat(featureNames{plotIt},':',num2str(macrotextureRegularity)))
    subplot(122)
    imshow(img);
    title('100 Strongest Feature Points from Surface Image');
    hold on;
    plot(selectStrongest(hcdPoints, 100));
    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperSize',[40 10.2]);                  %[12 4.2]);
    set(gcf,'PaperPosition',[-4.0,0.00,48,10.2]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    tmp = strsplit(ttitle,'.');
    print(gcf,'-dpdf',strcat('Visualization\\macrotextureRegularity_',tmp{1},'.pdf'));    

end





%% Anisotropy, Linelikeness, Directionality
%[MaRA]     = CBIR_LineLikeness(preprocessedImageNoFlash.grayI,ttitle,plotIt);




end

