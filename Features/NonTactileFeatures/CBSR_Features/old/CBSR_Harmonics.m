function [ cepstralHarmonics ] = CBSR_Harmonics( dataMovement, titleOfTexture, plotIt )
%   Description:        
%       This function calculates ...
%
%   Input:  data trace
%           title
%           flag for plotting
%   Output: 
%           ...
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................


    

    % Ensure correct number of inputs
    if( nargin~=  3), help CBSR_Harmonics; 
        return; 
    end; 
    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
       CBTR_Constants 
    end


    corrOffset = 100;
    
    X = abs(dct(dataMovement,CBSR_SAMPLERATE/2));
    specCorr = xcorr(X);
    specCorr = specCorr(corrOffset+CBSR_SAMPLERATE/2:end);
    specCorr(specCorr < 0) = 0;
    %figure;plot(specCorr);title(titleOfTexture);axis([0 10000 0 10^-1])
    frame = 1000;
    frameEnergies = [];
    for k = 1:frame:14000-frame
        tmp = 10000*(sum(abs(specCorr(k:k+frame))));
        frameEnergies = [frameEnergies tmp];
    end
    %figure;stem(frameEnergies);title(strcat('Cepstral Energies of...',titleOfTexture));axis([0 numel(frameEnergies) -0.5 inf])

    cepstralHarmonics = dct(log10(1+frameEnergies),numel(frameEnergies));
    if plotIt == 1
        figure;stem(cepstralHarmonics);title(strcat('Cepstral Harmonics of...',titleOfTexture));axis([0 numel(frameEnergies) -0.5 3])
    end

    

        
        



end






