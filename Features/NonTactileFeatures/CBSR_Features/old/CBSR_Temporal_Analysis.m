function [duration, zcr, hardness] = CBSR_Temporal_Analysis( impulseData )

% CBTR_HardnessAndFriction
%
%   Description
%
%
%
%   Inputs
%
%
%
%
%
%   Outputs
%
%
%
%
%
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: Andrea Sch�tt 2015

    %% PRELIMINARIES %% 
    
    duration = 0;
    zcr = 0;
    hardness = 0;
    

    % Ensure correct number of inputs
    if( nargin~= 1 ), help CBSR_Temporal_Analysis; 
        return; 
    end; 

    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
        CBTR_Constants
    end


    %% ______get "pure" tapping information and segmentation____%%
    
     duration = numel(impulseData);
    
    %% _____zero crossing rate zcr_____%%
    
      zcr = 0.5*mean(abs(diff(sign(impulseData(1:end)))));

    %% ________________ hardness approximation _________________ %
    
    hardness = Hardnessapproximation(impulseData);    
    

end


function [ hardness ] = Hardnessapproximation( impulseData )

    %figure; plot(impulseData)
    [maxValues,maxIndices] = sort(impulseData,'descend');

    % count peaks over 0.6*maxPeak threshold
    sumTemp = 1;
    maxValue = maxValues(1);
    for k=1:numel(impulseData)
        if (impulseData(k) > 0.6*maxValue)
           sumTemp = sumTemp + 1; 
        end
    end

    % calculate average inclination
    centroid = sum(maxIndices(1:sumTemp)) / sumTemp;  %SpectralCentroid(abs(impulseData),2000);
    maxHeight = sum(maxValues(1:sumTemp)) / sumTemp;
    riseInclination = abs(( maxHeight ) / ( centroid ));

    
    hardness = sqrt(  1*max(abs(impulseData)) + 10000 * riseInclination  );
   

end