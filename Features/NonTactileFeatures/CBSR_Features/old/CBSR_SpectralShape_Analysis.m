function [spread, skewness, slope, rolloff, specCentr, highSpecMean ] = CBSR_SpectralShape_Analysis( impulseData,ttitle,plotIt )

% CBTR_HardnessAndFriction
%
%   Description
%
%
%
%   Inputs
%
%
%
%
%
%   Outputs
%
%
%
%
%
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: Andrea Sch�tt 2015

%% PRELIMINARIES %%

spread      = 0;
slope       = 0;
skewness 	= 0;
rolloff     = 0;
specCentr   = 0;
highSpecMean= 0;
% Ensure correct number of inputs
if( nargin~= 3 ), help CBSR_SpectralShape_Analysis;
    return;
end;

% Check, if CBTR_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    CBTR_Constants
end




X = abs(dct(impulseData,CBSR_SAMPLERATE/2));


%% _____Spectral Shape_____%%

[spread, skewness, slope] = SpectralShape(X);

%% _____Spectral Roll-Off_____%%

[rolloff] = SpectralRollOff(X);


%% _____Spectral Centroid_____%%

specCentr = SpectralCentroid(X,44100);

%% _____Spectral Clamp Detection_____%%

highSpecMean = mean(X(4000:end));



if plotIt == 1
    figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SISS:',num2str(spread)));axis([0 8000 0 1.5])
end
if plotIt == 2
    figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SISSk:',num2str(skewness)));axis([0 8000 0 1.5])
end
if plotIt == 3
    figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SISSl:',num2str(slope)));axis([0 8000 0 1.5])
end
if plotIt == 4
    figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SISR:',num2str(rolloff)));axis([0 8000 0 1.5])
end
if plotIt == 5
    figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SISC:',num2str(specCentr)));axis([0 8000 0 1.5])
end
if plotIt == 6
    figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SISHM:',num2str(highSpecMean)));axis([0 8000 0 1.5])
end

end




function [ spread, skewness, slope ] = SpectralShape( X )
%   Description:
%       This function calculates the spectral slope, spread and skewness of
%       the given Signal
%   References
%
%           [1] http://www.audiocontentanalysis.org/code/audio-features/spectral-skewness/
%           [2] Paper "A large Set of Audio Features..."


spec = X;
centroid = SpectralCentroid(X,44100);



%% spread & skewness:
data = X.^2;
centr = centroid .* ones(numel(data),1);
P = data./sum(data);
X = linspace(0,numel(data)-1,numel(data))';
temp = X-centr;
%temp = temp ./ max(temp);
temp2 = temp.^2;
spread = sqrt(sum(temp2.*P));





    % compute skewness
    data       = data - repmat(centroid, size(data,1), 1);
    skewness   = sum ((X.^3)./(repmat(spread, size(X,1), 1).^3*size(X,1)));


skewness = log10(1+skewness);






%% slope

maxX = max(spec);
meanX = mean(spec);
slope = maxX / meanX;

end



function [ RollOffFrequency ] = SpectralRollOff( X )


Summing = 0;
i = 0;

%%  calculation

SignalPower = (X).^2;

RollOff = 0.95 * sum(SignalPower);

while Summing<RollOff
    i=i+1;
    Summing = Summing + SignalPower(i);
end

RollOffFrequency = i;


end








