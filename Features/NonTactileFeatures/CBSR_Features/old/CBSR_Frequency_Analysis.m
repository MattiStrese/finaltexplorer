function [distanceHL, f1, f2, sp1 ] = CBSR_Frequency_Analysis( impulseData, ttitle, plotIt )
%
%   Description
%
%
%
%   Inputs
%
%
%
%
%
%   Outputs
%
%
%
%
%
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: Andrea Sch�tt 2015

%% PRELIMINARIES %%
% Ensure correct number of inputs
if( nargin~= 3 ), help CBSR_Frequency_Analysis;
    return;
end;

% Check, if CBTR_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    CBTR_Constants
end



distanceHL  = 0;
f1          = 0;
f2          = 0;
sp1         = 0;

 X = abs(dct(impulseData,CBSR_SAMPLERATE/2));   


 
 
%% _____Feature 1: High and low frequency peaks______%
% old: [freqHL, distanceHL, ratioHL ] = highandlowtones(impulseData); 
 
[maxFreqValues1,~] = sort(X(1:3500),'descend');
[maxFreqValues2,~] = sort(X(3501:7000),'descend');

i1 = maxFreqValues1(1);
i2 = maxFreqValues2(1);

distanceHL = i1/i2;

    
%% _____Feature 2,3: Major Frequencies____%%
% old: [frequencies] = CBSR_MajorFrequencies(impulseData,'',0,5);

[maxFreqValues,maxFreqIndices] = sort(X,'descend');
f1 = maxFreqIndices(1);
f2 = maxFreqIndices(5);
f3 = maxFreqIndices(9);



%% _____Feature 4: Fundamental Frequency/Autocorrelation____%%

[~, ~, sp1, ~] = FFAutoCorr(X,plotIt);





if plotIt == 1
   figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SIHL:...',num2str(distanceHL)));axis([0 8000 0 1.5])
end
if plotIt == 2
    figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SIMF1:...',num2str(f1),'...SIMF2:...',num2str(f2)));axis([0 8000 0 1.5])
end
if plotIt == 3
    figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SISP:...',num2str(sp1)));axis([0 8000 0 1.5])
end



end






function [ freq, distance, ratio ] = highandlowtones( data )
%   Description:
%       This function calculates two maximum peaks in the spectrum separated
%       for a lower frequency region and a higher frequency region
%
%   Input:  data trace
%           title
%           flag for plotting
%   Output:
%           2 frequencies in high and low frequency region
%           their distance
%           their ratio

freq = [];
distance = 0;
cnt = 3;
previous = 1;
temp = 2;
ratio = 0;


%%  calculate something

frequencies = CBSR_MajorFrequencies(data,'',0,2);
freq = [frequencies(1) frequencies(2)];
distance = mean(abs(frequencies));
signal = abs(dct(data,44100/2));



CBSR_MajorFrequencies(data,'',0,cnt);
%%calculate ratio of intensity
power = (abs(dct(data,22050))).^2;
ratio = power(freq(1))/power(freq(2));



end










function [ ff1, ff2, schwerpkt,schwerpkt2 ] = FFAutoCorr( X,plotIt )

%   Description:
%       This function calculates the Autocorrelation Function and
%       appriximates the Fundamental Frequency
%
%   References
%
%           [1] http://cnx.org/contents/8b900091-908f-42ad-b93d-806415434b46@2/Pitch_Detection_Algorithms
%               cite as follows:
%               Gareth Middleton, Pitch Detection Algorithms. OpenStax CNX. Dec 17, 2003
%               http://cnx.org/contents/8b900091-908f-42ad-b93d-806415434b46@2@2.
%               23.01.2015

schwerpkt = 0;
schwerpkt2 = 0;
ff1 = 0;
ff2 = 0;

frame = 20; %50; % for ascend 1;
specframed= [];

%% calculation
spec = X;
for i=1:frame:numel(spec)-frame
    specframed = [specframed sum(spec(i:i+frame))];
end

AutoCorr = xcorr(specframed);


%% Die Autocorrelation hat das Maximum wenn es sich selbst in der
%Ursprungslage �hnelt, also schneide ich die vordere H�lfte ab und muss
%noch entscheiden ab wann ich die Maxima berechne.

tmp1 = round(numel(AutoCorr)/2.0);
tmp2 = round(numel(AutoCorr));

AutoCorrAll = AutoCorr( tmp1:tmp2 );
%xall = linspace(1,floor(numel(AutoCorrAll)),floor(numel(AutoCorrAll)));

thresh = mean(abs(diff(AutoCorrAll(1:numel(AutoCorrAll)*0.1))));
begin = find(abs(diff(AutoCorrAll))<thresh);
AutoCorr = AutoCorrAll(begin(1):end);
%figure;plot(AutoCorr); title('1')

x = linspace(1,numel(AutoCorr),numel(AutoCorr));


autocorr2 = diff(AutoCorr);
autocorr2(autocorr2 < 0) = 0;

%figure;plot(autocorr2); title('CBSRRequencyAnalysis')

%% Schwerpunkt
schwerpkt = sum([0:numel(AutoCorr)-1].*AutoCorr)./sum(AutoCorr)*frame;


%% Peaks entsprechen Arten der FundamentalFrequenz ...

%[pks,locs] = findpeaks(AutoCorr);
loc = findpeaksG(x,AutoCorr,0,max(AutoCorr)*0.01,2,3);

[~,sorted] = sort(loc(:,3),'descend');
ff1 = loc(sorted(1),2)*frame;

for i=2:numel(sorted)
    if loc(sorted(i),2)>numel(AutoCorr)*0.1
        ff2 = loc(sorted(i),2)*frame;
    end
end








%% Autokorrelation der Autokorrelation

AAC = xcorr(AutoCorr);
AAC = AAC(round(numel(AAC)/2):numel(AAC));
schwerpkt2 = sum([0:numel(AAC)-1].*AAC)./sum(AAC)*frame;
x2 = linspace(1,numel(AAC),numel(AAC));
loc2 = findpeaksG(x,AAC,0,max(AAC)*0.01,2,3);

% Plotten
if plotIt == 3
    figure;
    %plot(x,AutoCorr,'b',locs,pks,'og',schwerpkt,0,'sr');
    subplot(2,1,1)
    plot(x,AutoCorr,'b',loc(:,2),loc(:,3),'og',schwerpkt/frame,0,'sr');
    title(strcat('Auto Correlation with framesize ',num2str(frame), ', green Peaks and red Centroid'));
    subplot(2,1,2)
    plot(x2,AAC,'b',loc2(:,2),loc2(:,3),'og',schwerpkt2/frame,0,'sr');
    title('Auto Correlation of Autocorrelation with green Peaks and red Centroid');
end

end

%%

