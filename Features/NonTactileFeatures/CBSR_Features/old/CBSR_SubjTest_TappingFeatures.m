function [hardness, zcr, specCentroid, highToLowFreqRatio, highToMiddleFreqRatio] = CBSR_SubjTest_TappingFeatures( impulseData, ttitle, plotIt)

% 
%
%   Description
%
%
%
%   Inputs
%
%
%
%
%
%   Outputs
%
%
%
%
%
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%



    %% PRELIMINARIES %% 
    


    % Ensure correct number of inputs
    if( nargin~= 3 ), help CBSR_TappingFeatures; 
        return; 
    end; 

    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
        CBTR_Constants
    end

    
 

    
    
    X = abs(dct(impulseData,CBSR_SAMPLERATE/2));   

    hardness             = HardnessApproximation(impulseData);
    zcr                  = 0.5*mean(abs(diff(sign(impulseData(1:end)))));
    specCentroid         = SpectralCentroid(X,CBSR_SAMPLERATE);
    %specCentroid        = SpectralCentroid(filteredX,CBSR_SAMPLERATE)
    highToLowFreqRatio   = mean(X(4001:7000)) / mean(X(11:1000));
    highToMiddleFreqRatio= mean(X(4001:7000)) / mean(X(1001:4000)); 
    
    if plotIt == 1
        figure;plot(impulseData); title(strcat('Time Domain:',ttitle,'...SIH:',num2str(hardness)));axis([-inf inf, -1 1])
    end  
    if plotIt == 2
        figure;plot(impulseData); title(strcat('Time Domain:',ttitle,'...SIZCR:',num2str(zcr)));axis([-inf inf, -1 1])
    end  
    if plotIt == 3
        figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SISC:',num2str(specCentroid)));axis([0 8000 0 1.5])
    end  
    if plotIt == 4
        figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SIHL:',num2str(highToLowFreqRatio)));axis([0 8000 0 1])
    end  
    if plotIt == 5
        figure;plot(X,'black');title(strcat('Spectral Domain:',ttitle,'...SIML:',num2str(highToMiddleFreqRatio)));axis([0 8000 0 1])
    end     
    
    
    
end

function [ hardness ] = HardnessApproximation( impulseData )

    %figure; plot(impulseData)
    [maxValues,maxIndices] = sort(impulseData,'descend');

    % count peaks over 0.6*maxPeak threshold
    sumTemp = 1;
    maxValue = maxValues(1);
    for k=1:numel(impulseData)
        if (impulseData(k) > 0.6*maxValue)
           sumTemp = sumTemp + 1; 
        end
    end

    % calculate average inclination
    centroid = sum(maxIndices(1:sumTemp)) / sumTemp;  %SpectralCentroid(abs(impulseData),2000);
    maxHeight = sum(maxValues(1:sumTemp)) / sumTemp;
    riseInclination = abs(( maxHeight ) / ( centroid ));

    
    hardness = sqrt(  1*max(abs(impulseData)) + 10000 * riseInclination  );
   

end













