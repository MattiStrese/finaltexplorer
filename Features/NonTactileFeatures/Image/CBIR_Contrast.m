function [ contrastFeature ] = CBIR_Contrast( histogram,ttitle,plotIt )
%CBIR_CONTRAST Summary of this function goes here
%   Detailed explanation goes here

%   calculations as suggested by Albert Iepure, "Image Based
%   Haptic Feature Extraction" (Master Thesis)



    % omid flashlight effects
    histogram(250:end) = 0;
    
    
    k = 1:256;
    histCentroid = (k*histogram) /(sum(histogram));

    histCentroidVec = histCentroid .* ones(1,256);
    k2 = (k-histCentroidVec).^2;
    
    contrastFeature = sqrt(sum(k2*histogram) / sum(histogram));
    if plotIt == 1
        figure;stem(histogram); title(strcat(ttitle,'...SC = ',num2str(histCentroid),'...Spread = ',num2str(contrastFeature)));
    end
    
 
    
end

