function [ dominantColor,dominantSaturation ] = CBIR_DominantColor( colorI,ttitle,plotIt )
%CBIR_DOMINANTCOLOR Summary of this function goes here
%   Detailed explanation goes here



%B = colorspace('YCbCr<-RGB',colorI);
HSB = colorspace('HSB<-RGB',colorI);
Hue = HSB(:,:,1);
Saturation = HSB(:,:,2);


hueVector = Hue(:);
hueHist = hist(Hue(:),360);

%% TO DO   use hue histogramm to define color feature!!!!!!!!!!!!!!!!!!


if plotIt== 1
    figure; plot(hueHist); title(ttitle);
end

dominantColor = mean(Hue(:));%-mean(Brightness(:));
if dominantColor > 350
    dominantColor = 360-dominantColor;
end


dominantSaturation = mean(Saturation(:));






% cb = B(:,:,2);
% cb = cb(:)-128;
% cr = B(:,:,3);
% cr = cr(:)-128;
% 
% maxCB = sort(cb,'descend');
% minCB = sort(cb,'ascend');
% maxCR = sort(cr,'descend');
% minCR = sort(cr,'ascend');
% 
% meanMaxCB = mean(maxCB(1:1000));
% meanMinCB = mean(minCB(1:1000));
% 
% meanMaxCR = mean(maxCR(1:1000));
% meanMinCR = mean(minCR(1:1000));
% 
% 
% 
% if meanMaxCB > abs(meanMinCB)
%     meanCB = meanMaxCB;
% else
%     meanCB = meanMinCB;
% end
% if meanMaxCR > abs(meanMinCR)
%     meanCR = meanMaxCR;
% else
%     meanCR = meanMinCR;
% end
% 
% %scaler = abs(meanCB)/abs(meanCR);
% %meanCB = scaler*meanCB;
% 
% tmp  =(256*(rgb2gray(colorI)));
% grayVal = mean(tmp(:));
% 
% 
% 
% dominantColor = grayVal + ( 180 / 3.14159 * atan2(128+meanCR,128+meanCB));
% 
% 
% if plotIt == 1
%     
%     figure; plot(cb(:),cr(:),'xb'); hold on; plot(meanCB,meanCR,'or'); axis([-100 100 -100 100]); title(strcat(ttitle,'....Angle...',num2str(dominantColor)));
%     xlabel('Cb')
%     ylabel('Cr')
% 
% 
% %     figure;
% %     % View the individual channels
% %     subplot(2,2,1);
% %     imagesc(B(:,:,1));
% %     colormap(gray(256));
% %     axis image
% %     title 'Y'''
% %     subplot(2,2,3);
% %     imagesc(B(:,:,2));
% %     colormap(gray(256));
% %     axis image
% %     title C_b
% %     subplot(2,2,4);
% %     imagesc(B(:,:,3));
% %     colormap(gray(256));
% %     axis image
% %     title C_r
% 
% end


end

