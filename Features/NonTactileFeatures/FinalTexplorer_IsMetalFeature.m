function [ isMetallic ] = FinalTexplorer_IsMetalFeature( metalData, ttitle, plotIt )
%FINALTEXPLORER_ISMETALFEATURE Summary of this function goes here
%   Detailed explanation goes here



isMetallic = 0;


tmp = min(metalData);
metalData = metalData + abs(tmp);

if mean(metalData) > 0.05
    isMetallic = 1;
end


if plotIt == 16
   figure;
   plot(metalData,'black'); title(strcat(ttitle,'...',num2str(isMetallic)));

   axis([-inf inf -1.1 1.1])
   
    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperSize',[40 10.2]);                  %[12 4.2]);
    set(gcf,'PaperPosition',[-4.0,0.00,48,10.2]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    tmp = strsplit(ttitle,'.');
    %print(gcf,'-dpdf',strcat('Visualization\\WaG_',tmp{1},'.pdf'));
end


end

