function [ averagedFeatures  ] = FinalTexplorer_SoundFeatures( snd, ttitle, plotIt )
% 


%% PRELIMINARIES %%
snd = snd(:,1);

% Ensure correct number of inputs
%if( nargin~= 7 ), help CBTR_5Dim_MacroscopicRoughness;
%    return;
%end;
% Check, if CBTR_Constants has already been loaded for global variablclces
if ( any(strcmp(who,'numTextures')) == 0)
    FinalTexplorer_Constants
end


%% output
soundEnergy  = 0;

[ SND,f ] = CBSR_SignalSpectrum( snd', 44100, '', 2 );




%% ideas

% microtexture roughness, perhaps in combination with acceleration data to
% compensate the velocity

% stiffness -> sound impact loudness

% specific material detection

% 










featureNames =... 
{...
    'Zero Crossing Rate',... 
    'Signal Energy',...
    'Signal Energy Entropy',...
    'Spectral Centroid',...
    'Spectral Spread',...
    'SpecEntropy',...
    'SpcFlux',...
    'SpecRollOff',...
    'MFCC1',...
    'MFCC2',...
    'MFCC3',...
    'MFCC4',...
    'MFCC5',...
    'MFCC6',...
    'MFCC7',...
    'MFCC8',...
    'MFCC9',...
    'MFCC10',...
    'MFCC11',...
    'MFCC12',...
    'MFCC13',...
    'Spectral Harmonic HR',...
    'Spectral Harmonic F0',...
    'Chroma1',...
    'Chroma2',...
    'Chroma3',...
    'Chroma4',...
    'Chroma5',...
    'Chroma6',...
    'Chroma7',...
    'Chroma8',...
    'Chroma9',...
    'Chroma10',...
    'Chroma11',...
    'Chroma12'...  
};



frame = 0.5;
step = 0.1;
fs = 44100;
plotNr = plotIt;

% convert window length and step from seconds to samples:
windowLength    = round(frame * fs);
step            = round(step  * fs);
curPos          = 1;
L               = length(snd);
% compute the total number of frames:
numOfFrames     = floor((L-windowLength)/step) + 1;
% number of features to be computed:
numOfFeatures   = 35;
Features        = zeros(numOfFeatures, numOfFrames);
Ham             = window(@hamming, windowLength);
mfccParams      = feature_mfccs_init(windowLength, fs);

for i=1:numOfFrames % for each frame
    
    % get current frame:
    frame  = snd(curPos:curPos+windowLength-1);
    frame  = frame .* Ham;
    frameFFT = CBSR_SignalSpectrum(frame, fs, '',0);
    
    if (sum(abs(frame))>eps)
        % compute time-domain features:
        Features(1,i) = feature_zcr(frame);
        Features(2,i) = feature_energy(frame);
        Features(3,i) = feature_energy_entropy(frame, 10);

        % compute freq-domain features: 
        if (i==1) 
            frameFFTPrev = frameFFT; 
        end;
        
        [Features(4,i) Features(5,i)]   = feature_spectral_centroid(frameFFT, fs);
        Features(6,i)                   = feature_spectral_entropy(frameFFT, 10);
        Features(7,i)                   = feature_spectral_flux(frameFFT, frameFFTPrev);
        Features(8,i)                   = feature_spectral_rolloff(frameFFT, 0.90);
        MFCCs                           = feature_mfccs(frameFFT, mfccParams);
        Features(9:21,i)                = MFCCs;
        
        [HR, F0]                        = feature_harmonic(frame, fs);
        Features(22, i)                 = HR;
        Features(23, i)                 = F0;        
        Features(23+1:23+12, i)         = feature_chroma_vector(frame, fs);
    else
        Features(:,i)                   = zeros(numOfFeatures, 1);
    end  
    
    curPos = curPos + step;
    frameFFTPrev = frameFFT;
end

Features(35, :) = medfilt1(Features(35, :), 3);

if plotNr > 0 && plotNr < 36
   figure; plot(Features(plotNr,:),'black'); title(featureNames(plotNr)); 
end


averagedFeatures = zeros(35,1);
for k=1:35
    averagedFeatures(k,1) = median(sort(Features(k,:),'descend'));
end









if plotIt == 2
    figure;
    plot(snd); 
    %title(strcat(ttitle,'...',num2str(slipstickValue))); 
    axis([-inf inf 0 5])
    %hold on
    %plot(movAvgOfData,'r');
    %hold off
    xlabel('Sample','FontSize',12,'FontName','Arial');
    ylabel('Sound','FontSize',12,'FontName','Arial');

    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperSize',[20 5.2]);                  %[12 4.2]);
    set(gcf,'PaperPosition',[-2.0,0.00,24,5.2]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    tmp = strsplit(ttitle,'.');
    %print(gcf,'-dpdf',strcat('Features\\CBTR_5Dim\\FeaturesPDF\\MaRS_FS_',tmp{1},'.pdf'));
end








%% Feature) 









end

