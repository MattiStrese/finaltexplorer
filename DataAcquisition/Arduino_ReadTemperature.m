function [temperatureData] = Arduino_ReadTemperature(portname)
%ARDUINO_READTEMPERATURE Summary of this function goes here
%   Detailed explanation goes here
delete(instrfind)
%portname = 'COM74'
s = serial(portname); % 54    change the COM Port number as needed
s.baudrate = 9600;
%% Connect the serial port to Arduino
s.InputBufferSize = 20; % read only one byte every time
%try
    fopen(s);
% catch exc
%     try 
%     s = serial('COM3'); 
%     s.InputBufferSize = 20; % read only one byte every time
%     fopen(s);
%     catch exc
%         fclose(instrfind);
%         error('Make sure you select the correct COM Port where the Arduino is connected.');
%     end
% end

pause(4)

%% Connect the serial port to Arduino
s.InputBufferSize = 20; % read only one byte every time

%% Create a figure window to monitor the live data
Tmax = 20; % Total time for data collection (s)
figure,
grid on,
xlabel ('Time (s)'), ylabel('Temperature'),
axis([0 Tmax+1 15 50]),
%% Read and plot the data from Arduino
Ts = 0.01; % Sampling time (s)
i = 0;
data = 0;
t = 0;
tic % Start timer



while toc <= Tmax
    i = i + 1;
    %% Read buffer data
    tmp = str2double(fscanf(s));
   
    if isnan(tmp) == 1
        continue;
    end
    
    if tmp > 50
        continue
    end
    
    if tmp < 20
        tmp = 20;
        continue;
    end   
        data(i) = tmp;  %100*(tmp(1)-48)+  10*(tmp(2)-48)+   1*tmp(3)-48;
   

    %% Read time stamp
    % If reading faster than sampling rate, force sampling time.
    % If reading slower than sampling rate, nothing can be done.
    % Considerd
    % decreasing the set sampling time Ts
    t(i) = toc;
%     if i > 1
%         T = toc - t(i-1);
%         while T < Ts
%             T = toc - t(i-1);
%         end
%     end
%     t(i) = toc;
    %% Plot live data
    if i > 1
        line([t(i-1) t(i)],[data(i-1) data(i)])
        drawnow
    end
end
fclose(s);

temperatureData = data;

if(numel(temperatureData) < 2)
    temperatureData = zeros(100,1);
else


[maxVal,maxInd] = max(data);
tmp = data(maxInd:end);
close all
clc
Tmax = maxVal
Tmin = 26;






figure; plot(tmp)
for tt = 51:50:200
    tmp(tt);  
    y = (50./(tt-1)) * log( (Tmax-Tmin) ./ (tmp(tt) - Tmin))
end
mean(y)

end

end

