function [  ] = FinalTexplorer_Sensortest_FSR( duration )
if ( any(strcmp(who,'numTextures')) == 0)
    FinalTexplorer_Constants
end



channel1 = 'ai1'; % 'ai9';
channel2 = 'ai2';%'ai10';
channel3 = 'ai3';%'ai11';
channel4 = 'ai6';%'ai0';
channel5 = 'ai7';%'ai1';
channel6 = 'ai4';
channel7 = 'ai5';
channel8 = 'ai0';

s = daq.createSession('ni')
%% Acceleration
ch1 = addAnalogInputChannel(s,devName, channel1, 'Voltage');
ch2 = addAnalogInputChannel(s,devName, channel2, 'Voltage');
ch3 = addAnalogInputChannel(s,devName, channel3, 'Voltage');
%% Friction
ch4 = addAnalogInputChannel(s,devName, channel4, 'Voltage');
ch5 = addAnalogInputChannel(s,devName, channel5, 'Voltage');
%% Reflectance
ch6 = addAnalogInputChannel(s,devName, channel6, 'Voltage');
%% Metal Detection
ch7 = addAnalogInputChannel(s,devName, channel7, 'Voltage');
%% Normal Force Detection
ch8 = addAnalogInputChannel(s,devName, channel8, 'Voltage');

ch1.TerminalConfig = 'SingleEnded';
ch2.TerminalConfig = 'SingleEnded';
ch3.TerminalConfig = 'SingleEnded';
ch4.TerminalConfig = 'SingleEnded';
ch5.TerminalConfig = 'SingleEnded';
ch6.TerminalConfig = 'SingleEnded';
ch7.TerminalConfig = 'SingleEnded';
ch8.TerminalConfig = 'SingleEnded';

s.Rate = FS_OTHER_SENSORS;
s.DurationInSeconds = duration;


% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...in 2s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...in 1s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...now!'));
% disp('-------------------------');
input('Press Enter: FSR Test')


calibrationScan = s.inputSingleScan

recObj = audiorecorder(44100,16,2);
record(recObj, duration);

[data,time] = s.startForeground;

stop(recObj);
% Store data in double-precision array.
soundRecording = getaudiodata(recObj);


time   = time(100:end);

FSR1Friction = data(100:end,4);
FSR2Friction = data(100:end,5);
normalForce  = data(100:end,8);

FSR1Friction = FSR_Linearization(FSR1Friction);
FSR2Friction = FSR_Linearization(FSR2Friction);
normalForce  = FSR_Linearization(normalForce);

reflectance = data(100:end,6);



    figure('units','normalized','outerposition',[0 0 1 1]);

    
    subplot(211)
    plot(FSR1Friction,'blue'); hold on
    plot(FSR2Friction,'green');
    xlabel('Time (secs)');
    ylabel('gram')
    title('Friction Forces Difference');
    %axis([-inf inf -0.2 5000]);
    %plot(abs(FSR2Friction-FSR1Friction),'black');
    
    subplot(212)
    plot(normalForce,'red');
    legend({'FSR1','FSR2','Delta FSR','Normal',})
    xlabel('Time (secs)');
    ylabel('gram')
    title('Normal Force');


end

