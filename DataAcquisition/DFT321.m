function [ dft321,dft321DampedHandAccel ] = DFT321( a,b,c, usePaperDefinition )

%% Example

% t = 0.0001:0.0001:10;
% y1 = sin(250*3.1415*t);
% y2 = sin(50*3.1415*t);
% y3 = sin(500*3.1415*t);
% [e,f] = DFT321(y1,y2,y3);
% stem(abs(dct(e,10000)))

%%

	A 		= fft(a,numel(a));
	B 		= fft(b,numel(b));
	C 		= fft(c,numel(c));

	magA   	= abs(A);
	phaseA 	= angle(A);
	magB   	= abs(B);
	phaseB 	= angle(B);
	magC   	= abs(C);
	phaseC 	= angle(C);


	magDFT321   							= sqrt(power(magA,2) + power(magB,2) + power(magC,2));
	magDFT321DampedHandAccel 				= magDFT321;
	magDFT321DampedHandAccel(1:10) 			= sqrt(magDFT321(1:10));
	magDFT321DampedHandAccel(end-10:end) 	= sqrt(magDFT321(end-10:end));
    
    if usePaperDefinition == 0
        phaseDFT321 = phaseA + phaseB + phaseC;
    else
        phaseDFT321 = angle(A+B+C);
    end
        
	DFT321 									= magDFT321.*exp(phaseDFT321*sqrt(-1));
	DFT321DampedHandAccel 					= magDFT321DampedHandAccel.*exp(phaseDFT321*sqrt(-1));

	dft321 = real( ifft(DFT321) );
	dft321DampedHandAccel  = real( ifft(DFT321DampedHandAccel) );

end

