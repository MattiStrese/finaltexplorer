   

    fs = 10000;
    channel1 = 'ai13'; % 'ai9';
    channel2 = 'ai14';%'ai10';
    channel3 = 'ai15';%'ai11';
    channel4 = 'ai11';%'ai0';
    channel5 = 'ai10';%'ai1';
    channel6 = 'ai9';
    channel7 = 'ai12';
%     channel7 = 'ai10';
%     channel8 = 'ai11';    
    
    s = daq.createSession('ni')
    %% Acceleration
    addAnalogInputChannel(s,'Dev1', channel1, 'Voltage');
    addAnalogInputChannel(s,'Dev1', channel2, 'Voltage');
    addAnalogInputChannel(s,'Dev1', channel3, 'Voltage');
    %% Friction
    addAnalogInputChannel(s,'Dev1', channel4, 'Voltage');
    addAnalogInputChannel(s,'Dev1', channel5, 'Voltage');
    %% Reflectance
	addAnalogInputChannel(s,'Dev1', channel6, 'Voltage');
    %% Metal Detection
    addAnalogInputChannel(s,'Dev1', channel7, 'Voltage');
%     addAnalogInputChannel(s,'Dev1', channel8, 'Voltage');
    s.Rate = fs;
    s.DurationInSeconds = duration;

    s

    calibrationScan = s.inputSingleScan
    
    
    [data,time] = s.startForeground;
    
    %% FSR compensation
    %data(:,4) = abs(data(:,4) - calibrationScan(1,4))
    %data(:,5) = abs(data(:,5) - calibrationScan(1,5))
    
    