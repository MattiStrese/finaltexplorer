function [ images ] = CBTR_CaptureImages( surfaceNumber, numberOfPictures, isFlash, saveRecordings )
%CBTR_CAPTUREIMAGES Summary of this function goes here
%   Detailed explanation goes here


    tmp = load('labelNames108.mat');
    y = unique(tmp.labelNames108);  


    images = ImageRecorder(numberOfPictures);
    
    pause(1)
    
    
    % only equals 1 if no live recording
    if saveRecordings == 1
        disp('')
        disp(y{surfaceNumber})
        disp('')
        
        if isFlash == 0
            for k=1:numberOfPictures
                disp(strcat('Database/Recordings108/ImageScans/NoFlash/',y{surfaceNumber},'_Image_',num2str(k),'.png'))
                imwrite(images{k},strcat('Database/Recordings108/ImageScans/NoFlash/',y{surfaceNumber},'_Image_',num2str(k),'.png'));
            end
        else
            for k=1:numberOfPictures
                disp(strcat('Database/Recordings108/ImageScans/','Flash_',y{surfaceNumber},'_Image_',num2str(k),'.png'))
                imwrite(images{k},strcat('Database/Recordings108/ImageScans/','Flash_',y{surfaceNumber},'_Image_',num2str(k),'.png'));
            end
        end
        
    end
    
end

