recObj = audiorecorder(44100,16,2);
disp('Start Audiorecording.')
recordblocking(recObj, duration);
disp('End of Recording.');

% Play back the recording.
%play(recObj);

% Store data in double-precision array.
myRecording = getaudiodata(recObj);

