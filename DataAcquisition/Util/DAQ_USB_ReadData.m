function [ output_args ] = DAQ_USB_ReadData( input_args )
%DAQ_USB_READDATA Summary of this function goes here
%   Detailed explanation goes here
s = daq.createSession('ni')
addAnalogInputChannel(s,'Dev2', 'ai3', 'Voltage');
s.Rate = 10000;
s.DurationInSeconds = 3;
[data,time] = s.startForeground;
plot(data(:,1))

end

