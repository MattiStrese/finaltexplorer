function [ y ] = CBTR_ConvertFSR( x )
    y = 5 * ( 1 - 0.1969 * (  (10.^(x/5)) - 1 ) ).^4;
end

