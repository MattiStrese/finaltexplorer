   

    fs = 1420;
    channel1 = 'ai0'; % 'ai9';
    channel2 = 'ai4';%'ai10';
    channel3 = 'ai1';%'ai11';
    channel4 = 'ai5';%'ai0';
    channel5 = 'ai2';%'ai1';
    channel6 = 'ai7';
    channel7 = 'ai3';

 
     devName = 'Dev1';
    
    s = daq.createSession('ni')
    %% Acceleration
    ch1 = addAnalogInputChannel(s,devName, channel1, 'Voltage');
    ch2 = addAnalogInputChannel(s,devName, channel2, 'Voltage');
    ch3 = addAnalogInputChannel(s,devName, channel3, 'Voltage');
    %% Friction
    ch4 = addAnalogInputChannel(s,devName, channel4, 'Voltage');
    ch5 = addAnalogInputChannel(s,devName, channel5, 'Voltage');
    %% Reflectance
	ch6 = addAnalogInputChannel(s,devName, channel6, 'Voltage');
    %% Metal Detection
    ch7 = addAnalogInputChannel(s,devName, channel7, 'Voltage');
    
    ch1.TerminalConfig = 'SingleEnded';    
    ch2.TerminalConfig = 'SingleEnded';    
    ch3.TerminalConfig = 'SingleEnded';    
    ch4.TerminalConfig = 'SingleEnded';    
    ch5.TerminalConfig = 'SingleEnded';    
    ch6.TerminalConfig = 'SingleEnded';    
    ch7.TerminalConfig = 'SingleEnded';   
    
    s.Rate = fs;
    s.DurationInSeconds = duration;



    calibrationScan = s.inputSingleScan
    
    
    [data,time] = s.startForeground;
    
%     %% FSR compensation
%     data(:,4) = abs(data(:,4) - calibrationScan(1,4));
%     data(:,5) = abs(data(:,5) - calibrationScan(1,5));
%     
%     figure; plot(data(:,1)); title('Accel')
%     axis([-inf inf -0.1 5.5])
%     hold on;
%     plot(data(:,2));
%     plot(data(:,3));
%     hold off
%     
%     figure; plot(data(:,4));  title('Friction')
%     axis([-inf inf -0.1 5.5])
%     hold on;
%     plot(data(:,5));
%     hold off
% 
%     figure; plot(data(:,6));  title('Rfl and Metal')
%     axis([-inf inf -0.1 5.5])
%     hold on;
%     plot(data(:,7));
%     hold off
    

    
    
    