function [ output_args ] = ScanFSR( scanNumber,duration,printIt,plotIt )
%SCANACCELEROMETER Summary of this function goes here
%   Detailed explanation goes here

    fSensor = @(x) 15.311*exp(0.005199*x);
    %fSensorInv = @(x) 0.005199^-1 * log(1/15.311 * x)

    fs = 10000;

    channel4 = 'ai11';%'ai0';
    channel5 = 'ai10';%'ai1';

    numQueries = 10;    
    
    y = load('labelNames.mat');
    y = y.labelNames;

    if printIt == 1
        devices = daq.getDevices
    end
    %daq.reset;
    s = daq.createSession('ni')

    addAnalogInputChannel(s,'Dev1', channel4, 'Voltage');
    addAnalogInputChannel(s,'Dev1', channel5, 'Voltage');
    
    s.Rate = fs;
    s.DurationInSeconds = duration;

    s

    clc
    disp('-------------------------');
    disp('New Measurement in     3s');
    disp('-------------------------');
    pause(1);
    clc
    disp('-------------------------');
    disp('New Measurement in     2s');
    disp('-------------------------');
    pause(1);   
    clc
    disp('-------------------------');
    disp('New Measurement in     1s');
    disp('-------------------------');
    pause(1);
    clc
    disp('-------------------------');
    disp('New Measurement      NOW!');
    disp('-------------------------'); 

    [data,time] = s.startForeground;
    


    
    
    time   = time(100:end);
  
    %figure;plot((abs(data(100:end,4))))
    %figure;plot((abs(data(100:end,5))))
    
    rawFsr1 = data(100:end,1);
    rawFsr2 = data(100:end,2);
    
    
%     fsr1 = (1 - 0.1969*abs(data(100:end,1))).^4.0 ;
%     fsr2 = (1 - 0.1969*abs(data(100:end,2))).^4.0 ;  
%     FSR1Friction  = 5 * fsr1;
%     FSR2Friction  = 5 * fsr2;
    
    
    FSR1Friction  = 5 * log10(1+ data(100:end,1));
    FSR2Friction  = 5 * log10(1+ data(100:end,2));  

    if plotIt == 1
        figure;plot(FSR1Friction); hold on
        plot(FSR2Friction,'r')
        xlabel('Time (secs)');
        ylabel('Delta Voltage') 
        title(y{10*scanNumber-5});
        axis([-inf inf -0.5 5.5]);
     

    end
    

     
    saveStrFric1   = strcat('FricScans/',y{10*scanNumber-5},'_FrictionFSR1_Movement.txt');
    saveStrFric2   = strcat('FricScans/',y{10*scanNumber-5},'_FrictionFSR2_Movement.txt');
    saveStrFric1raw   = strcat('FricScans/RAW_',y{10*scanNumber-5},'_FrictionFSR1_Movement.txt');
    saveStrFric2raw   = strcat('FricScans/RAW_',y{10*scanNumber-5},'_FrictionFSR2_Movement.txt');
       
  

 
    save(saveStrFric1,'FSR1Friction','-ASCII');
    save(saveStrFric2,'FSR2Friction','-ASCII');
    
    save(saveStrFric1raw,'rawFsr1','-ASCII');
    save(saveStrFric2raw,'rawFsr2','-ASCII');
    
    

end

