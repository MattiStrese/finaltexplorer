   

    fs = 1420;
    channel1 = 'ai1'; % 'ai9';
    channel2 = 'ai2';%'ai10';
    channel3 = 'ai3';%'ai11';
    channel4 = 'ai6';%'ai0';
    channel5 = 'ai7';%'ai1';
    channel6 = 'ai4';
    channel7 = 'ai5';

    
    s = daq.createSession('ni')
    %% Acceleration
    addAnalogInputChannel(s,'Dev2', channel1, 'Voltage');
    addAnalogInputChannel(s,'Dev2', channel2, 'Voltage');
    addAnalogInputChannel(s,'Dev2', channel3, 'Voltage');
    %% Friction
    addAnalogInputChannel(s,'Dev2', channel4, 'Voltage');
    addAnalogInputChannel(s,'Dev2', channel5, 'Voltage');
    %% Reflectance
	addAnalogInputChannel(s,'Dev2', channel6, 'Voltage');
    %% Metal Detection
    addAnalogInputChannel(s,'Dev2', channel7, 'Voltage');
%     addAnalogInputChannel(s,'Dev1', channel8, 'Voltage');
    s.Rate = fs;
    s.DurationInSeconds = duration;



    calibrationScan = s.inputSingleScan
    
    
    [data,time] = s.startForeground;
    
%     %% FSR compensation
%     data(:,4) = abs(data(:,4) - calibrationScan(1,4));
%     data(:,5) = abs(data(:,5) - calibrationScan(1,5));
%     
%     figure; plot(data(:,1)); title('Accel')
%     axis([-inf inf -0.1 5.5])
%     hold on;
%     plot(data(:,2));
%     plot(data(:,3));
%     hold off
%     
%     figure; plot(data(:,4));  title('Friction')
%     axis([-inf inf -0.1 5.5])
%     hold on;
%     plot(data(:,5));
%     hold off
% 
%     figure; plot(data(:,6));  title('Rfl and Metal')
%     axis([-inf inf -0.1 5.5])
%     hold on;
%     plot(data(:,7));
%     hold off
    

    
    
    