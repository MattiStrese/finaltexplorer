function [ images ] = ImageRecorder( numberOfPictures, showImage )
%IMAGERECORDER Summary of this function goes here
%   Detailed explanation goes here

images = {};

webcamlist
try
cam = webcam('USB Microscope'); % (1) or (2)
catch exc
 cam = webcam('Teslong Endoscope'); % (1) or (2)
   
end
%cam.AvailableResolutions;
% try
%      cam.resolution = '1280x720';
% catch exc
%     disp(exc)
% end
preview(cam);

%figure;
for k=1:numberOfPictures
    prompt = 'Press Enter to capture an image! ';
    x = input(prompt);
    img = snapshot(cam);
 
    images{k} = img;
    %image(img)
end
closePreview(cam)
clear('cam');

if showImage > 0
    figure; imshow(images{1})
end




end

