function [ materialRecording ] = FinalTexplorer_DoSingleMeasurement( surfaceID, duration, scanNumber, plotIt, taskName, saveIt )
%SCANACCELEROMETER Summary of this function goes here
%   Detailed explanation goes here
%accelX,accelY,accelZ, accelDFT, normalForce,FSR1Friction,FSR2Friction,reflectance,soundRecording,metalDetection, temperatureData, img


if ( any(strcmp(who,'numTextures')) == 0)
    FinalTexplorer_Constants
end


materialRecording = struct;








%% 0 Image Acquisition
% disp('-------------------------');
% disp(strcat('Visual Recording:','...in 2s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Visual Recording:','...in 1s'));
% disp('-------------------------');
% pause(1);
disp('-------------------------');
disp('Visual Recording');
disp('-------------------------');
img = ImageRecorder(1,1);






%% 1 Static Contact
% disp('-------------------------');
% disp(strcat('Static Contact:','...in 2s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Static Contact:','...in 1s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Static Contact:','...now!'));
% disp('-------------------------');
input('Press Enter: Static Contact 1')

temperatureData = Arduino_ReadTemperature(PORT);
close all

%temperatureData= ones(1000,1);

pause(1);











%% Acceleration Acquisition: Tapping

channel1 = 'ai1'; % 'ai9';
channel2 = 'ai2';%'ai10';
channel3 = 'ai3';%'ai11';
s = daq.createSession('ni')
%% Acceleration
ch1 = addAnalogInputChannel(s,devName, channel1, 'Voltage');
ch2 = addAnalogInputChannel(s,devName, channel2, 'Voltage');
ch3 = addAnalogInputChannel(s,devName, channel3, 'Voltage');
ch1.TerminalConfig = 'SingleEnded';
ch2.TerminalConfig = 'SingleEnded';
ch3.TerminalConfig = 'SingleEnded';
s.Rate = FS_ACCEL;
s.DurationInSeconds = duration;


% disp('-------------------------');
% disp(strcat('Tapping Recording:','...in 2s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Tapping Recording:','...in 1s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Tapping Recording:','...now!'));
% disp('-------------------------');
input('Press Enter: Tapping (Acceleration and Sound)')

calibrationScan = s.inputSingleScan

recObj = audiorecorder(FS_SOUND,16,2);
disp('Start Audio Recording.')
record(recObj, duration);

[data,time] = s.startForeground;

stop(recObj);
soundTapRecording = getaudiodata(recObj);
disp('End of Recordings.');



time   = time(100:end);
accelTapX = (data(100:end,1)- mean(data(100:end,1)));
accelTapY = (data(100:end,2)- mean(data(100:end,2)));
accelTapZ = (data(100:end,3)- mean(data(100:end,3)));








%% Sliding: Acceleration and Sound

channel1 = 'ai1'; % 'ai9';
channel2 = 'ai2';%'ai10';
channel3 = 'ai3';%'ai11';
s = daq.createSession('ni')
%% Acceleration
ch1 = addAnalogInputChannel(s,devName, channel1, 'Voltage');
ch2 = addAnalogInputChannel(s,devName, channel2, 'Voltage');
ch3 = addAnalogInputChannel(s,devName, channel3, 'Voltage');
ch1.TerminalConfig = 'SingleEnded';
ch2.TerminalConfig = 'SingleEnded';
ch3.TerminalConfig = 'SingleEnded';
s.Rate = FS_ACCEL;
s.DurationInSeconds = duration;


% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...in 2s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...in 1s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...now!'));
% disp('-------------------------');
input('Press Enter: Sliding (Acceleration and Sound)')



calibrationScan = s.inputSingleScan

recObj = audiorecorder(FS_SOUND,16,2);
disp('Start Recording.')
record(recObj, duration);

[data,time] = s.startForeground;

stop(recObj);
soundRecording = getaudiodata(recObj);
disp('End of Recordings.');

time   = time(100:end);
accelX = (data(100:end,1)- mean(data(100:end,1)));
accelY = (data(100:end,2)- mean(data(100:end,2)));
accelZ = (data(100:end,3)- mean(data(100:end,3)));

accelDFT = DFT321(accelX,accelY,accelZ,1);







%%  Sliding: Contour

channel1 = 'ai1'; % 'ai9';
channel2 = 'ai2';%'ai10';
channel3 = 'ai3';%'ai11';
channel4 = 'ai6';%'ai0';
channel5 = 'ai7';%'ai1';
channel6 = 'ai4';
channel7 = 'ai5';
channel8 = 'ai0';

s = daq.createSession('ni')
%% Acceleration
ch1 = addAnalogInputChannel(s,devName, channel1, 'Voltage');
ch2 = addAnalogInputChannel(s,devName, channel2, 'Voltage');
ch3 = addAnalogInputChannel(s,devName, channel3, 'Voltage');
%% Friction
ch4 = addAnalogInputChannel(s,devName, channel4, 'Voltage');
ch5 = addAnalogInputChannel(s,devName, channel5, 'Voltage');
%% Reflectance
ch6 = addAnalogInputChannel(s,devName, channel6, 'Voltage');
%% Metal Detection
ch7 = addAnalogInputChannel(s,devName, channel7, 'Voltage');
%% Normal Force Detection
ch8 = addAnalogInputChannel(s,devName, channel8, 'Voltage');

ch1.TerminalConfig = 'SingleEnded';
ch2.TerminalConfig = 'SingleEnded';
ch3.TerminalConfig = 'SingleEnded';
ch4.TerminalConfig = 'SingleEnded';
ch5.TerminalConfig = 'SingleEnded';
ch6.TerminalConfig = 'SingleEnded';
ch7.TerminalConfig = 'SingleEnded';
ch8.TerminalConfig = 'SingleEnded';

s.Rate = FS_OTHER_SENSORS;
s.DurationInSeconds = duration;


% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...in 2s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...in 1s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...now!'));
% disp('-------------------------');
input('Press Enter: Sliding (Contour)')


calibrationScan = s.inputSingleScan

%recObj = audiorecorder(44100,16,2);
%record(recObj, duration);

[data,time] = s.startForeground;

%stop(recObj);
% Store data in double-precision array.
%soundRecording = getaudiodata(recObj);


time   = time(100:end);

FSR1Friction = data(100:end,4);
FSR2Friction = data(100:end,5);
normalForce  = data(100:end,8);

FSR1Friction = FSR_Linearization(FSR1Friction);
FSR2Friction = FSR_Linearization(FSR2Friction);
normalForce  = FSR_Linearization(normalForce);

reflectance = data(100:end,6);








% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...in 2s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...in 1s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Free Exploration Recording:','...now!'));
% disp('-------------------------');
input('Press Enter: Sliding (Friction)')


calibrationScan = s.inputSingleScan

recObj = audiorecorder(44100,16,2);


[data,time] = s.startForeground;




time   = time(100:end);

FSR1Friction = data(100:end,4);
FSR2Friction = data(100:end,5);
normalForce  = data(100:end,8);

FSR1Friction = FSR_Linearization(FSR1Friction);
FSR2Friction = FSR_Linearization(FSR2Friction);
normalForce  = FSR_Linearization(normalForce);

















%% Stiffness
channel1 = 'ai1'; % 'ai9';
channel2 = 'ai2';%'ai10';
channel3 = 'ai3';%'ai11';
channel4 = 'ai6';%'ai0';
channel5 = 'ai7';%'ai1';
channel6 = 'ai4';
channel7 = 'ai5';
channel8 = 'ai0';

s = daq.createSession('ni')
%% Acceleration
ch1 = addAnalogInputChannel(s,devName, channel1, 'Voltage');
ch2 = addAnalogInputChannel(s,devName, channel2, 'Voltage');
ch3 = addAnalogInputChannel(s,devName, channel3, 'Voltage');
%% Friction
ch4 = addAnalogInputChannel(s,devName, channel4, 'Voltage');
ch5 = addAnalogInputChannel(s,devName, channel5, 'Voltage');
%% Reflectance
ch6 = addAnalogInputChannel(s,devName, channel6, 'Voltage');
%% Metal Detection
ch7 = addAnalogInputChannel(s,devName, channel7, 'Voltage');
%% Normal Force Detection
ch8 = addAnalogInputChannel(s,devName, channel8, 'Voltage');

ch1.TerminalConfig = 'SingleEnded';
ch2.TerminalConfig = 'SingleEnded';
ch3.TerminalConfig = 'SingleEnded';
ch4.TerminalConfig = 'SingleEnded';
ch5.TerminalConfig = 'SingleEnded';
ch6.TerminalConfig = 'SingleEnded';
ch7.TerminalConfig = 'SingleEnded';
ch8.TerminalConfig = 'SingleEnded';

s.Rate = FS_OTHER_SENSORS;
s.DurationInSeconds = 3;


% disp('-------------------------');
% disp(strcat('Stiffness:',taskName,'...in 2s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Stiffness:',taskName,'...in 1s'));
% disp('-------------------------');
% pause(1);
% disp('-------------------------');
% disp(strcat('Stiffness:',taskName,'...now!'));
% disp('-------------------------');
input('Press Enter: Stiffness')


calibrationScan = s.inputSingleScan

recObj = audiorecorder(44100,16,2);
disp('Start Other sensor recording.')
record(recObj, duration);

[data,time] = s.startForeground;

stop(recObj);

disp('End of Recording.');

disp(strcat(taskName,' finished.'));

%% Set result data


time   = time(100:end);
%metalDetection = -(1/2.5) * (data(100:end,7)-2.53);
stiffnessData  = FSR_Linearization(data(100:end,8));
stiffnessData2  = FSR_Linearization(data(100:end,4));




input('Press Enter: Static Contact 2')


%% Static 2
channel1 = 'ai1'; % 'ai9';
channel2 = 'ai2';%'ai10';
channel3 = 'ai3';%'ai11';
channel4 = 'ai6';%'ai0';
channel5 = 'ai7';%'ai1';
channel6 = 'ai4';
channel7 = 'ai5';
channel8 = 'ai0';

s = daq.createSession('ni')
%% Acceleration
ch1 = addAnalogInputChannel(s,devName, channel1, 'Voltage');
ch2 = addAnalogInputChannel(s,devName, channel2, 'Voltage');
ch3 = addAnalogInputChannel(s,devName, channel3, 'Voltage');
%% Friction
ch4 = addAnalogInputChannel(s,devName, channel4, 'Voltage');
ch5 = addAnalogInputChannel(s,devName, channel5, 'Voltage');
%% Reflectance
ch6 = addAnalogInputChannel(s,devName, channel6, 'Voltage');
%% Metal Detection
ch7 = addAnalogInputChannel(s,devName, channel7, 'Voltage');
%% Normal Force Detection
ch8 = addAnalogInputChannel(s,devName, channel8, 'Voltage');

ch1.TerminalConfig = 'SingleEnded';
ch2.TerminalConfig = 'SingleEnded';
ch3.TerminalConfig = 'SingleEnded';
ch4.TerminalConfig = 'SingleEnded';
ch5.TerminalConfig = 'SingleEnded';
ch6.TerminalConfig = 'SingleEnded';
ch7.TerminalConfig = 'SingleEnded';
ch8.TerminalConfig = 'SingleEnded';

s.Rate = FS_OTHER_SENSORS;
s.DurationInSeconds = 3;


disp('Start sensor recording.')

[data,time] = s.startForeground;

disp('End of Recording.');

%% Set result data

time   = time(100:end);
metalDetection = -(1/2.5) * (data(100:end,7)-2.53);












%% plot section

if plotIt == 1
    
    figure('units','normalized','outerposition',[0 0 0.9 0.9]);
    %figure;
    subplot(511)
    plot(temperatureData) 
    xlabel('Sample');
    ylabel('Temperature (�)');
    title('Thermal Conductivity Estimation');
    axis([-inf inf 10 46]);  
    subplot(512)
    plot(soundRecording);
    xlabel('Time (secs)');
    ylabel('Voltage')
    title('Sound Exploration');
    axis([-inf inf -0.5 0.5]);    
    subplot(513)
    plot(stiffnessData);
    hold on
    plot(stiffnessData2,'r');
    xlabel('Time (secs)');
    ylabel('Mass (g)')
    title('Stiffness');
    %axis([-inf inf 5000 7000]);   
   
    
    subplot(514)
    plot(accelTapX)

    hold on;
    plot(accelTapY);
    plot(accelTapZ);
    hold off
    xlabel('Time (secs)');
    ylabel('Voltage');
    title('Acceleration Tapping');
    axis([-inf inf -1.5 1.5]); 
    
    subplot(515)
    plot(soundTapRecording);
    xlabel('Time (secs)');
    ylabel('Voltage')
    title('Sound');
    axis([-inf inf -0.5 0.5]);   
    
    
    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperSize',[40 10.2]);                  %[12 4.2]);
    set(gcf,'PaperPosition',[-4.0,0.00,48,10.2]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    print(gcf,'-dpdf',strcat('Visualization\\RawPlot...',y{surfaceID},'_1.pdf'));

    
    
    
    
    
    figure('units','normalized','outerposition',[0 0 0.9 0.9]);
    %figure;
    subplot(511)
    plot(accelDFT)
    %plot(accelX);
    %hold on;
    %plot(accelY);
    %plot(accelZ);
    %hold off
    xlabel('Time (secs)');
    ylabel('Voltage');
    title('Acceleration Exploration DFT321');
    axis([-inf inf -1.5 1.5]);
    
    
    
    subplot(512)
    plot(FSR1Friction,'blue'); hold on
    plot(FSR2Friction,'green');
    xlabel('Time (secs)');
    ylabel('gram')
    title('Friction Forces Difference');
    axis([-inf inf -0.2 5000]);
    %plot(abs(FSR2Friction-FSR1Friction),'black');
    
    subplot(513)
    plot(normalForce,'red');
    legend({'FSR1','FSR2','Delta FSR','Normal',})
    xlabel('Time (secs)');
    ylabel('gram')
    title('Normal Force');
   % axis([-inf inf 4500 7000]);
    
    
    subplot(514)
    plot(reflectance);
    xlabel('Time (secs)');
    ylabel('Voltage')
    title('Reflectance');
    axis([-inf inf 0 5]);
    
    
    subplot(515)
    plot(metalDetection)
    xlabel('Time (secs)');
    ylabel('Voltage')
    title('Metal Detection');
    axis([-inf inf -1.5 1.5]);
    

    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperSize',[40 10.2]);                  %[12 4.2]);
    set(gcf,'PaperPosition',[-4.0,0.00,48,10.2]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    print(gcf,'-dpdf',strcat('Visualization\\RawPlot...',y{surfaceID},'_2.pdf'));

   
end




materialRecording.accelX = accelX;
materialRecording.accelY = accelY;
materialRecording.accelZ = accelZ; 
materialRecording.accelTapX = accelTapX;
materialRecording.accelTapY = accelTapY;
materialRecording.accelTapZ = accelTapZ; 
materialRecording.accelDFT = accelDFT;
%materialRecording.normalForce = normalForce;
materialRecording.stiffness   = stiffnessData;
materialRecording.stiffness2 = stiffnessData2;
materialRecording.FSR1Friction = FSR1Friction;
materialRecording.FSR2Friction = FSR2Friction;
materialRecording.reflectance = reflectance;
materialRecording.soundRecording = soundRecording;
materialRecording.soundTapRecording = soundTapRecording;
materialRecording.metalDetection = metalDetection;
materialRecording.temperatureData = temperatureData;
materialRecording.img = img;

materialRecording.scanNumber = scanNumber;

save(strcat(materialMatPath,y{surfaceID},'_',num2str(scanNumber),'.mat'),'materialRecording');



if saveIt == 1
    
    %saveStrAccelDFT321                = strcat(folderName,'/AccelScansDFT321/',y{surfaceID},'_',subjectName,'.txt');%,'_query',num2str(mod(surfaceID,numQueries)),'.txt');
    %saveStrAccelDFT321Paper           = strcat(folderName,'/AccelScansDFT321Paper/',y{surfaceID},'_',subjectName,'.txt');%,'_query',num2str(mod(surfaceID,numQueries)),'.txt');
    %saveStrAccelDFT321Paper50ms       = strcat(folderName,'/AccelScansDFT321Paper50ms/',y{surfaceID},'_',subjectName,'.txt');%,'_query',num2str(mod(surfaceID,numQueries)),'.txt');
    
    % accel
    saveStrAccelX           = strcat(accPath,y{surfaceID},'_',num2str(scanNumber),'_X.txt');%_query',num2str(mod(surfaceID,numQueries)),'.txt');
    saveStrAccelY           = strcat(accPath,y{surfaceID},'_',num2str(scanNumber),'_Y.txt');%_query',num2str(mod(surfaceID,numQueries)),'.txt');
    saveStrAccelZ           = strcat(accPath,y{surfaceID},'_',num2str(scanNumber),'_Z.txt');%_query',num2str(mod(surfaceID,numQueries)),'.txt');
    saveStrAccelDFT321      = strcat(accPath,y{surfaceID},'_',num2str(scanNumber),'_DFT321.txt');
    
    saveStrAccelTapX           = strcat(accTapPath,y{surfaceID},'_',num2str(scanNumber),'_tapX.txt');%_query',num2str(mod(surfaceID,numQueries)),'.txt');
    saveStrAccelTapY           = strcat(accTapPath,y{surfaceID},'_',num2str(scanNumber),'_tapY.txt');%_query',num2str(mod(surfaceID,numQueries)),'.txt');
    saveStrAccelTapZ           = strcat(accTapPath,y{surfaceID},'_',num2str(scanNumber),'_tapZ.txt');%_query',num2str(mod(surfaceID,numQueries)),'.txt');

    
    % friction
    saveStrFric1            = strcat(fricPath,y{surfaceID},'_',num2str(scanNumber),'_FSR1','.txt');%,,num2str(mod(surfaceID,numQueries)),'.txt');
    saveStrFric2            = strcat(fricPath,y{surfaceID},'_',num2str(scanNumber),'_FSR2','.txt');%,,num2str(mod(surfaceID,numQueries)),'.txt');
    
    % stiffness
    saveStrFricN            = strcat(stiffnessPath,y{surfaceID},'_',num2str(scanNumber),'_FSRNorm','.txt');%,,num2str(mod(surfaceID,numQueries)),'.txt');
     
    % sound
    saveStrSound            = strcat(soundPath,y{surfaceID},'_',num2str(scanNumber),'_Sound','.wav');%,num2str(mod(surfaceID,numQueries)),'.wav');
    saveStrSoundTap         = strcat(soundTapPath,y{surfaceID},'_',num2str(scanNumber),'_SoundTap','.wav');%,num2str(mod(surfaceID,numQueries)),'.wav');
    
    % reflectance
    saveStrReflectance      = strcat(reflectancePath,y{surfaceID},'_',num2str(scanNumber),'_Reflectance','.txt');%,num2str(mod(surfaceID,numQueries)),'.wav');
    
    % metal
    saveStrMetalDetection   = strcat(metalPath,y{surfaceID},'_',num2str(scanNumber),'_MetalDetection','.txt');
   
    % thermal
    saveStrThermal          = strcat(thermalPath,y{surfaceID},'_',num2str(scanNumber),'_Thermal','.txt');
     
    % image
    saveStrImage            = strcat(imagePath,y{surfaceID},'_',num2str(scanNumber),'_Image','.png');


    %% Save recorded signals
    save(saveStrAccelX,'accelX','-ASCII');
    save(saveStrAccelY,'accelY','-ASCII');
    save(saveStrAccelZ,'accelZ','-ASCII');
    save(saveStrAccelDFT321,'accelDFT','-ASCII');
    save(saveStrAccelTapX,'accelTapX','-ASCII');
    save(saveStrAccelTapY,'accelTapY','-ASCII');
    save(saveStrAccelTapZ,'accelTapZ','-ASCII');

    save(saveStrFric1,'FSR1Friction','-ASCII');
    save(saveStrFric2,'FSR2Friction','-ASCII');
    save(saveStrFricN,'normalForce','-ASCII');
   
    audiowrite(saveStrSound,soundRecording,FS_SOUND);
    audiowrite(saveStrSoundTap,soundTapRecording,FS_SOUND);

    save(saveStrReflectance,'reflectance','-ASCII');
    save(saveStrMetalDetection,'metalDetection','-ASCII');
    save(saveStrThermal,'temperatureData','-ASCII');

    imwrite(img{1},saveStrImage);

end



disp('...done.')
end











