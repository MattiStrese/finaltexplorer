function [ video ] = VideoRecorder( numberOfPictures, showImage )
%IMAGERECORDER Summary of this function goes here
%   Detailed explanation goes here

video = {};

webcamlist
try
cam = webcam('USB Microscope'); % (1) or (2)
catch exc
 cam = webcam('Teslong Endoscope'); % (1) or (2)
end
%cam.AvailableResolutions;
% try
%      cam.resolution = '1280x720';
% catch exc
%     disp(exc)
% end
preview(cam);

%figure;
    prompt = 'Press Enter to capture a video! ';
    x = input(prompt);
for k=1:numberOfPictures
    disp(strcat(num2str(100*(k/numberOfPictures)),'% done.'));
    img = snapshot(cam);
 
    images{k} = img;
    %image(img)
end
closePreview(cam)
clear('cam');

if showImage > 0
    figure; imshow(images{1})
    figure; imshow(images{30})
    figure; imshow(images{60})
    figure; imshow(images{90})
    
end




end

