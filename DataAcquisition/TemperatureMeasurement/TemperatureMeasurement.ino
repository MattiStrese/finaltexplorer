
#include <Wire.h>
#include <Adafruit_MLX90614.h>

Adafruit_MLX90614 mlx = Adafruit_MLX90614();


#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // twelve servo objects can be created on most boards
 
int pos = 0;    // variable to store the servo position 
 

int RelayControl1 = 4;    // Digital Arduino Pin used to control the motor
 
int button1 = 10;

int buttonState = 0;         // variable for reading the pushbutton status 
int oldButtonState = 0;         // variable for reading the pushbutton status 


int doOnce = 0;

void setup()  
{
    pinMode(RelayControl1, OUTPUT);
    pinMode(button1, INPUT_PULLUP);

  Serial.begin(9600);

  //Serial.println("Adafruit MLX90614 test");  


    digitalWrite(RelayControl1,HIGH);// NO1 and COM1 Connected (LED on)
    delay(3000); // wait 5000 milliseconds (5  second)
    digitalWrite(RelayControl1,LOW);// NO1 and COM1 Disconnected (LED off)


  mlx.begin();  

  myservo.attach(9);  // attaches the servo on pin 9 to the servo object 

  myservo.write(50);

  
}
 
 
void loop()  
{
  buttonState = digitalRead(button1);
  //Serial.println(buttonState);  
  if (buttonState != oldButtonState) 
  {
    oldButtonState = buttonState;
    myservo.write(0);
    digitalWrite(RelayControl1,HIGH);// NO1 and COM1 Connected (LED on)
    delay(3000); // wait 5000 milliseconds (5  second)
    digitalWrite(RelayControl1,LOW);// NO1 and COM1 Disconnected (LED off)
    myservo.write(50);
    //Serial.println("Button pressed.");  
  }

  delay(20);
  double val = mlx.readObjectTempC();//round(10*(15+mlx.readAmbientTempC() - mlx.readObjectTempC()));
  //val = map(val, -100,100, 255, 200);

  Serial.println(val);

  //Serial.print("Ambient = "); Serial.print(mlx.readAmbientTempC()); 
  //Serial.print("*C\tObject = "); Serial.print(mlx.readObjectTempC()); Serial.println("*C");
  //Serial.println();
}
